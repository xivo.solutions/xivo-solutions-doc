.. _desktop-application-installation:

************
Installation
************

The **UC Assistant** and **CC Agent** are available as desktop application through Electron packaging, to be able to use these applications in a standalone executable you need to retrieve it on the login page.

You can retrieve it by clicking the Windows or Linux button on the login page from your browser, on the CCAgent or UCAssistant applications.

.. figure:: download-desktop.png

.. note::

          You can disable download buttons, see :ref:`disable_download_buttons` section.

Those applications are retrieved from the official XiVO Solutions mirror https://mirror.xivo.solutions/xivo-desktop-assistant/

******
Update
******

On Windows, the update is automatic. On start, if XiVOCC version has changed, it will fetch any update available on the mirror.
Then, you will have a message notifying you that the new version is installed, and asking you if you want to restart the application.

On Linux, you can retrieve the deb package from the mirror. You can easily access the correct version by clicking on the login page download button through a web browser.
