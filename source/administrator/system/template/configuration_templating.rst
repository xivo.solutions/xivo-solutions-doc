.. _configuration_templating:

************************************
xivo-config configuration templating
************************************

Concept
=======

| The xivo-config package is deployed on xivo pbx and on mds.
| It embeds default templates (files and directories) for various configuration on the machine.
| Those configuration can be safely overriden and extended as described below. 
| They are applied on calling the script **xivo-update-config**.

Provided configs
================

- dhcp
- mail
- provd
- system
- dialplan (xivo pbx & mds, new with NAOS)

Dialplan specifically is loaded to **/usr/share/xivo-config/dialplan/asterisk**, is checked after being loaded and can be configured on MDS and XiVO.
The other confs work only on XiVO.

Behavior
========

| xivo-config installs the config templates in the **base template directory** : **/usr/share/xivo-config/templates**
| For each config, there is a directory and .restart file which contains what to do once the config is updated
| You should not modify this folder, as it might be altered by future xivo-config updates
| Config modifications should be defined instead in the **custom template directory** : **/etc/xivo/custom-templates**
| You shall replicate/add there the file(s) you want to customized using the same structure as in base template directory. Check the examples.

On running **xivo-update-config**, the files from those directories will be thrown together and sent to the system directory where their configuration is applied.
For files existing in both directory only the custom version will be taken.

.. figure:: xivo-config-templating.drawio.png

..  code-block:: bash

    root@xivo-naos:~# xivo-update-config
    Generating configuration
    (Re)templating
    'dhcp' configuration OK, skipping.
    'dialplan' configuration OK, skipping.
    File asterisk/dirtyLeftover.conf lives outside the templated dialplan configuration
    'mail' configuration OK, skipping.
    'provd' configuration UPDATED.
    'system' configuration OK, skipping.
    (Re)configuring dahdi
    (Re)configuring dhcp
    DONE

Notes:

- the build folder is temporary
- **xivo-update-config** does not destroy anything. It's up to you to clean the target of dirty/old config files


Variable replacement
====================

To be detailed

Examples
========

Override existing dialplan conf
-------------------------------

* Copy/Create the file you want from the base templates folder to the custom template folder
  
  ..  code-block:: bash

        mkdir -p /etc/xivo/custom-templates/dialplan/asterisk
        cp /usr/share/xivo-config/templates/dialplan/asterisk/extensions_lib_did.conf /etc/xivo/custom-templates/dialplan/asterisk/
    
* Edit the file in custom-templates
* Run **xivo-update-config** (dialplan will be reloaded)
* The modified file is loaded to /usr/share/xivo-config/templates/asterisk/ & you can verify your modifications from asterisk cli

Override existing dhcp conf
---------------------------

* Copy/Create the file you want from the base templates folder to the custom template folder
  
  ..  code-block:: bash

        mkdir -p /etc/xivo/custom-templates/dhcp/etc/default/dhcp
        cp /usr/share/xivo-config/templates/dhcp/etc/default/dhcp/dhcp.conf /etc/xivo/custom-templates/dhcp/etc/default/dhcp/
    
* Edit the file in custom-templates
* Run **xivo-update-config**
* The modified file is loaded to /etc/default/dhcp/dhcp.conf
