Xivo-solutions-doc
==================

This is the documentation project of the XiVO Solutions based upon ReadTheDocs. It is available at <http://xivosolutions.readthedocs.io/>.

Usage
-----

This documentation is built with the following process :  

- run `make clean html`. Documentation is constructed under `build/`  
- open `build/html/index.html` in your browser. You may get the full path using `readlink -f build/html/index.html`
- your changes in `source/` are now reflected in the browser


If you encounter requirements issues, use debian builder (<https://gitlab.com/xivo.solutions/debian-builder>).

- Run it with `debuilder maia`. (see debuilder's readme).
- then run `make clean html` and open the index.html as described above

Requirements
------------

The makefile uses python 3 to create the virtualenv, you will need:

- python3 and pip3 installed on your host
- to install virtualenv using pip3

Dependencies
------------

- Sphinx
- sphinx-rtd-theme

They are pinned in requirements.in  
To generate requirements.txt with the transitive dependencies (i.e the dependencies of these main packages) we used pip-tools (see [here](https://docs.readthedocs.io/en/stable/guides/reproducible-builds.html#pin-your-transitive-dependencies))

To rebuild requirements.txt (best to do it in debuilder) :

```sh
make virtualenv-clean virtualenv
envs/bin/pip install pip-tools
envs/bin/pip-compile
```

Build
-----

   make clean html

PDF version
-----------

You will need a LATEX compilation suite. On Debian, you can use the following
packages :

$ apt-get install texlive-latex-base texlive-latex-recommended
texlive-latex-extra texlive-fonts-recommended

Troubleshooting
---------------

If the build fails with version errors, try removing the current cached env:

```sh
    make virtualenv-clean
```
