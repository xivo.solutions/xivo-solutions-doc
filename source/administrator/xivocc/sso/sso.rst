***
SSO
***

Single Sign-On (SSO) automates the authentication process entirely, requiring no action from the user's end.
For instance, technologies like Kerberos seamlessly authenticate the user by retrieving session information from Windows, eliminating the need for the user to enter their login credentials.
SSO enhances user experience by providing seamless and secure access to various services and applications without repetitive logins.

.. contents:: :local:

.. _kerberos-configuration:

Kerberos Authentication
=======================

To enable Kerberos authentication and single sign on feature, you need to have an existing Kerberos infrastructure with a
Key Distribution Center and a Ticket Granting Service. You need to be able to create a service, construct a kerberos server configuration and export a keytab to perform the following configuration. This service must be on the kerberos realm used by your users and must match the dns name of the server hosting the XUC server (or the nginx reverse proxy server if you use one). For example, assuming you have a realm named ``MYDOMAIN``, you can create a service named ``HTTP/xuc.mydomain`` and a dns entry for ``xuc.mydomain`` pointing the server hosting the XUC.

.. warning::

   The created domain name must be trusted by the user's browser.

Prerequisites
-------------

* Create a service for the XUC host, for example:

  .. code-block:: shell

                  addprinc HTTP/xuc.mydomain

* Export the keytab file, for example:

  .. code-block:: shell

                  ktadd -k xuc.keytab HTTP/xuc.mydomain


.. warning::

   The bash commands detailed here are for demonstration only and needs to be adapted to your specific environment. It shows how to create a service for the XUC Server named HTTP/xuc, associated to the example realm ``mydomain``.

   Only the following encryption types are supported by XiVOCC:

   - aes256-cts-hmac-sha1-96
   - arcfour-hmac
   - des3-cbc-sha1
   - des-cbc-crc


XiVOCC Configuration
--------------------

* Copy the previously generated ``xuc.keytab`` keytab file to the server hosting the XUC docker container, for example: ``/etc/docker/kerberos/xuc.keytab``.
* Create or edit the file ``/etc/krb5.conf`` on the server hosting the XUC docker container and change settings according to your kerberos environment. For example, the file may contain (name and ip addresses must match your kerberos environment):

  .. code-block:: ini

     [libdefaults]
         default_realm = MYDOMAIN

     [realms]
         MYDOMAIN = {
             kdc = 172.17.0.14
             admin_server = 172.17.0.14
         }

* Edit the docker compose file ``/etc/docker/compose/docker-xivocc.yml`` to add the following configuration in the xuc section (file name, service name, password may differ on your setup):

  .. code-block:: dockerfile


                  xuc:
                    # ...
                    environment:
                    - JAVA_OPTS=-Dsecured.krb5.principal=HTTP/xuc.mydomain -Dsecured.krb5.password=xuc -Dsecured.krb5.keyTab=/etc/kerberos/xuc.keytab

                    # ...

                    volumes:
                    - /etc/docker/kerberos:/etc/kerberos
                    - /etc/krb5.conf:/etc/krb5.conf


* Enable ``Single Sign On`` on the Agent, Manager, Web and Desktop application interface. Change the value of the following environment variables in the `/etc/docker/compose/custom.env`:

  .. code-block:: ini

     # ...
     USE_SSO=true
     XUC_HOST=xuc.mydomain
     # ...


Browser configuration
---------------------

The created domain name must be trusted by the user’s browser.

For Chrome (windows):

- Internet Option : Add domain with protocol to the list of trusted sites : http://xuc.mydomain (and/or https://xuc.mydomain ).


.. warning::

   Kerberos authentication on Chrome is only available on Microsoft Windows.

For Firefox:

- Go to ``about:config``
- add domain (without protocol) to the ``network.negotiate-auth.delegation-uris`` entry (ie. ``xuc.mydomain``).
- add domain (without protocol) to the ``network.negotiate-auth.trusted-uris`` entry (ie. ``xuc.mydomain``).
