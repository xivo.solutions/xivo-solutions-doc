.. _asterisk_sip:

**************
Asterisk & SIP
**************

Asterisk default SIP channel driver is since XiVO Izar *PJSIP*.

.. note:: Please note the :ref:`upgrade_chan_sip_pjsip_migration_guide`.

.. contents:: :local:

PJSIP Configuration
===================

.. note:: This applies to >=Jabbah.07, >= Kuma.03 and forward

Where the configuration is stored ?
-----------------------------------

The pjsip configuration is stored in :file:`/etc/asterisk/pjsip.d/01-pjsip.conf`.

When the configuration is generated ?
-------------------------------------

.. warning:: Since *Jabbah.07* the conf is only re-generated after an edition in the Webi.

   Launching manually a ``module reload res_pjsip.so`` in asterisk CLI **will not**
   reload the **current** conf from the db, it will only re-load the current generated conf
   (see :ref:`asterisk_sip_conf_reload`).

When you save an object in the Admin Web interface (a User, or a Line or ...) the Web Interface calls
``xivo-sysconfd`` service which will reload the configuration.


How the configuration is generated ?
------------------------------------

#. Admin changes some configuration in Webi
#. Webi calls ``xivo-sysconfd`` service to ask for a ``pjsip`` reload
#. Then ``xivo-sysconfd``:

   #. Calls a script (:file:`/usr/sbin/xivo-generate-pjsip-conf`) which itself calls ``xivo-confgen`` to generate pjsip conf
   #. Calls ``xivo-fix-paths-rights`` script to ensure correct rights
   #. Then puts the genertated conf in :file:`/etc/asterisk/pjsip.d/01-pjsip.conf`
   #. When it's done, launch a module reload: ``module reload res_pjsip.so``
#. And finally asterisk re-reads its pjsip conf

.. _asterisk_sip_conf_customization:

How to customize the configuration ?
------------------------------------

If you want to customize the configuration:

#. add your own configuration file in the directory :file:`/etc/asterisk/pjsip.d/`

   .. note:: Follow the `asterisk documentation
      on how to override an existing section
      <https://wiki.asterisk.org/wiki/display/AST/Adding+to+an+existing+section>`_.
#. then see :ref:`asterisk_sip_conf_reload`

.. _asterisk_sip_conf_reload:

How to reload the configuration ?
---------------------------------

Two cases:

#. Reload the **generated** configuration (it will also reload what you might have changed in :file:`pjsip.d` dir - see :ref:`asterisk_sip_conf_customization`), run::

    asterisk -rx 'module reload res_pjsip.so'

#. Reload the **current** configuration (from the db and from :file:`pjsip.d` dir) - otherly said, regenerate & reload the whole configuration::

    curl --insecure -XPOST -H 'Content-Type: application/json' 'http://localhost:8668/exec_request_handlers' -d '{"ipbx": ["module reload res_pjsip.so"]}'


Debugging
=========

See useful links:

- The upgrade notes on the dialplan differences: :ref:`upgrade_chan_sip_pjsip_migration_guide`
- https://www.asterisk.org/new-pjsip-logging-functionality/
- https://wiki.asterisk.org/wiki/display/AST/Asterisk+PJSIP+Troubleshooting+Guide


.. _asterisk_sip_chan_sip_fallback:

Fallback to ``chan_sip`` SIP channel driver
===========================================

.. warning:: If you chose to fallback to ``chan_sip`` you will need to redo it for each bugfix upgrade.


For Izar LTS a fallback mechanism has been implemented to be able to fallback to deprecated ``chan_sip`` SIP channel driver.

On your XiVO, run the following command to switch your XiVO from ``res_pjsip`` to ``chan_sip``::

  xivo-switch-sip-driver SIP

And follow the instructions displayed.

.. note:: If you're running an XDS installation you MUST switch all the XiVO and MDS of the installation to the same
   SIP channel driver.

