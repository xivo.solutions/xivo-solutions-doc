.. _xivosolutions_release:

*************
Release Notes
*************

.. _orion_release:

Orion
=====

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Naos (2024.10).


New Features
------------

**UC App**

* Group membership visualization added in UC Assistant see :ref:`uc_assistant_groups`
* **WebRTC**

  * Answer/Hangup call with compatible Headset - see :ref:`headset_call_control`

**Edge**

* It is now possible to expose TURNS on port 443 - see :ref:`edge_feature_proxy_turns_on_port_443`

   .. note:: Currently it is only usable for Meetingrooms - see :ref:`meetingrooms_features_turns_443`

**Meetingrooms**

* Update Jitsi to version 9909

  * We updated our Jitsi integration policy: previously, only basic functionalities were enabled. Now, all features are enabled except those that are not working (e.g., sharing links).
  * Introduced new Jitsi features: breakout rooms, local recording in Chrome, and more.



Behavior Changes
----------------

**UC App**

* Icon for meetingroom was changed from |old_image| to |new_image|

.. |old_image| image:: images/meeting_room_before.png
   :scale: 100%

.. |new_image| image:: images/meeting_room_after.png
   :scale: 100%

**Meetingrooms**

* Update Jitsi to version 9909:

  * Video calls are now supported on Firefox.
  * The ENABLE_BACKGROUND_SELECTION config key is removed. Background selection is now enabled by default
  * We updated our Jitsi integration policy: previously, only basic functionalities were enabled. Now, all features are enabled except those that are not working (e.g., sharing links).
  * Re-enabled self-view in conference rooms. Users can revert to the previous behavior in Jitsi options.
  * Removed unnecessary optimizations from the Jitsi configuration, as they are no longer needed in the new Jitsi version.


**Recording**

* The recording file starts at the first recorded segment of a call. If the recording is paused during the call, then a blank is present in the recording file for the duration of the pause (previously, those blank would be cut leading to such pause to be harder to track).


Deprecations
------------

This release deprecates:

  * `LTS Izar (2022.05) <https://documentation.xivo.solutions/en/2022.05/>`_ : This version is no longer supported.

Upgrade
-------

.. _upgrade_lts_manual_steps:

**Manual steps for LTS upgrade**

.. warning:: **Don't forget** to read carefully the specific steps to upgrade from another LTS version

.. toctree::
   :maxdepth: 1

   upgrade_from_izar_to_jabbah
   upgrade_from_jabbah_to_kuma
   upgrade_from_kuma_to_luna
   upgrade_from_luna_to_maia
   upgrade_from_maia_to_naos
   upgrade_from_naos_to_orion

**Generic upgrade procedure**

Then, follow the generic upgrade procedures:

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`

Naos Bugfixes Versions
======================

Components version table
------------------------

Table listing the current version of the components.

.. Note:: Because the **SpagoBI** technology is growing old and does not receive open-source updates, we won't build our SpagoBI image after the one from **Maia: 2024.05.latest**. That image will still work just as before for further releases, starting with Naos.

+---------------------+--------------+
| Component           | current ver. |
+=====================+==============+
| **XiVO**            |              |
+---------------------+--------------+
| XiVO PBX            | 2025.05.00   |
+---------------------+--------------+
| config_mgt          | 2025.05.00   |
+---------------------+--------------+
| db                  | 2025.05.00   |
+---------------------+--------------+
| outcall             | 2025.05.00   |
+---------------------+--------------+
| db_replic           | 2025.05.00   |
+---------------------+--------------+
| nginx               | 2025.05.00   |
+---------------------+--------------+
| webi                | 2025.05.00   |
+---------------------+--------------+
| switchboard_reports | 2025.05.00   |
+---------------------+--------------+
| usage_writer        | 2025.05.00   |
+---------------------+--------------+
| usage_collector     | 2025.05.00   |
+---------------------+--------------+
| agid                | 2025.05.00   |
+---------------------+--------------+
| confgend            | 2025.05.00   |
+---------------------+--------------+
| asterisk            | 8:20.6.0-1   |
+---------------------+--------------+
| docker-ce           | 5:25.0.5     |
+---------------------+--------------+
| docker-compose      | 2.26.1       |
+---------------------+--------------+
| **XiVO CC**         |              |
+---------------------+--------------+
| mattermost          | 2025.05.00   |
+---------------------+--------------+
| nginx               | 2025.05.00   |
+---------------------+--------------+
| pack-reporting      | 2025.05.00   |
+---------------------+--------------+
| pgxivocc            | 2025.05.00   |
+---------------------+--------------+
| recording-rsync     | 2025.05.00   |
+---------------------+--------------+
| recording-server    | 2025.05.00   |
+---------------------+--------------+
| spagobi             |**2024.05.00**|
+---------------------+--------------+
| xivo-full-stats     | 2025.05.00   |
+---------------------+--------------+
| xuc                 | 2025.05.00   |
+---------------------+--------------+
| xucmgt              | 2025.05.00   |
+---------------------+--------------+
| **Edge**            |              |
+---------------------+--------------+
| edge                | 2025.05.00   |
+---------------------+--------------+
| nginx               | 2025.05.00   |
+---------------------+--------------+
| kamailio            | 2025.05.00   |
+---------------------+--------------+
| coturn              | 2025.05.00   |
+---------------------+--------------+
| **Meeting Rooms**   |              |
+---------------------+--------------+
| meetingroom         | 2025.05.00   |
+---------------------+--------------+
| web-jitsi           | 2025.05.00   |
+---------------------+--------------+
| jicofo-jitsi        | 2025.05.00   |
+---------------------+--------------+
| prosody-jitsi       | 2025.05.00   |
+---------------------+--------------+
| jvb-jitsi           | 2025.05.00   |
+---------------------+--------------+
| jigasi-jitsi        | 2025.05.00   |
+---------------------+--------------+
| **IVR**             |              |
+---------------------+--------------+
| ivr-editor          | 2025.05.00   |
+---------------------+--------------+

Orion Intermediate Versions
===========================

.. toctree::
   :maxdepth: 2

   orion_iv
