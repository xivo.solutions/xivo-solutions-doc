.. _meetingrooms:

**************************
Meeting Rooms :badge:`PRO`
**************************

Meeting Rooms is a new feature introduced in the Helios LTS.

.. important:: This is an enterprise version feature that is not included in XiVO and is not freely available.
    To enable it, please contact `XiVO team <https://www.xivo.solutions/contact/>`_.

It introduces Video Conference in XiVO.

.. toctree::
   :maxdepth: 2

   install
   configuration
   features
   users_guide
   admins_guide
