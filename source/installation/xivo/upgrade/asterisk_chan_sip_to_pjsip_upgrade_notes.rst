.. _upgrade_chan_sip_pjsip_migration_guide:

**************************************************
Asterisk ``chan_sip`` to ``pjsip`` Migration Guide
**************************************************

During upgrade to Izar, the SIP channel driver is switched from ``chan_sip`` to ``res_pjsip``.

.. contents:: :local:

.. note:: For Izar LTS, rollback to ``chan_sip`` is supported. For this, see :ref:`asterisk_sip_chan_sip_fallback`.


Migration Guide
===============

This section describes some of the checks you have to do and documents some changes you'll have to make.

Manipulating SIP Headers
------------------------

With ``chan_sip`` you could use:

- dialplan application `SIPAddHeader <https://wiki.asterisk.org/wiki/display/AST/Asterisk+18+Application_SIPAddHeader>`_ to add SIP headers
- dialplan application `SIPRemoveHeader <https://wiki.asterisk.org/wiki/display/AST/Asterisk+18+Application_SIPRemoveHeader>`_ to remove SIP headers
- and dialplan function `SIP_HEADER <https://wiki.asterisk.org/wiki/display/AST/Asterisk+18+Function_SIP_HEADER>`_ to read SIP headers

If you use one of them in one of your custom dialplan you **have to change them** to use the dialplan function PJSIP_HEADER_ (please read carefully the PJSIP_HEADER_ documentation).

Major difference whith PJSIP_HEADER function
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. important:: PJSIP_HEADER_ dialplan function comes with the major difference (compared to SIPAddHeader or SIPRemoveHeader) to operate on the
   current channel (i.e. the **caller's** channel). See detail below.

The major difference between SIPAddHeader or SIPRemoveHeader and PJSIP_HEADER_ is that PJSIP_HEADER_ operates on the current channel (i.e. **caller's incoming** channel): ``PJSIP_HEADER(add,X-My-Hdr,2)`` will add the header ``X-My-Hdr`` on the **caller's** channel.

If you want to add a header on the (still to be created) **callee's outgoing** channel, you must use pre-dial handler. See example in PJSIP_HEADER_ documentation.

Utility functions in XiVO dialplan
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Note that currently XiVO supplies utility subroutines to do these read/add/remove action whatever the SIP channel driver:

- ``xivo-generic-sip-get-header`` which takes the following arg:

  - arg1: the name of the SIP Header to read
  - arg2: the name of the dialplan variable in which the header content will be set
- ``xivo-generic-sip-remove-header`` which takes the following arg:

  - arg1: the name of the SIP Header to remove

+---------------------------------------------------------------------------------------+
| Reading a SIP Header                                                                  |
+=======================================================================================+
| Old dialplan                                                                          |
+---------------------------------------------------------------------------------------+
| .. code-block:: ini                                                                   |
|                                                                                       |
|   same  = n,Set(MY_CONTEXT=${SIP_HEADER(X-My-Context)})                               |
|                                                                                       |
+---------------------------------------------------------------------------------------+
| New dialplan starting from Izar, compatible with ``res_pjsip`` (and ``chan_sip``)     |
+---------------------------------------------------------------------------------------+
| .. code-block:: ini                                                                   |
|                                                                                       |
|   same  = n,GoSub(xivo-generic-sip-get-header,s,1(X-My-Context,MY_CONTEXT))           |
|                                                                                       |
+---------------------------------------------------------------------------------------+


+---------------------------------------------------------------------------------------+
| Removing a SIP Header                                                                 |
+=======================================================================================+
| Old dialplan                                                                          |
+---------------------------------------------------------------------------------------+
| .. code-block:: ini                                                                   |
|                                                                                       |
|   same  = n,SIPRemoveHeader(X-My-Context)                                             |
|                                                                                       |
+---------------------------------------------------------------------------------------+
| New dialplan starting from Izar, compatible with ``res_pjsip`` (and ``chan_sip``)     |
+---------------------------------------------------------------------------------------+
| .. code-block:: ini                                                                   |
|                                                                                       |
|   same  = n,GoSub(xivo-generic-sip-remove-header,s,1(X-My-Context))                   |
|                                                                                       |
+---------------------------------------------------------------------------------------+

.. _upgrade_chan_sip_pjsip_migration_guide_hdr_mgt:

XiVO header manager for simplified PJSIP header management
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The module is named ``xivo_header_mgr`` and is available anywhere in the dialplan.
You can call it anywhere in your dialplan to add your headers except in predial handler options ``b()``.
It works with any SIP channel driver. It will store the modifications and apply then inside a predial handler already set in the default dialplan.

.. warning:: **Important limitation:** Since the predial handler was added by default in every ``Dial`` application calls, calling a predial handler in your
  preprocess subroutines will have the priority over the default one and will most likely break the header manager. You will need to call gosub ``set_header_on_channel``
  in your predial handler to make it work.

  +---------------------------------------------------------------------------------------+
  | Adding a customized preprocess subroutine                                             |
  +=======================================================================================+
  | Old dialplan                                                                          |
  +---------------------------------------------------------------------------------------+
  | .. code-block:: ini                                                                   |
  |                                                                                       |
  |    [my-pre-dial-handler]                                                              |
  |    exten = s,1,NoOp()                                                                 |
  |    same = n,...                                                                       |
  |    same = Return()                                                                    |
  |                                                                                       |
  |    [my-subr]                                                                          |
  |    exten = s,1,NoOp()                                                                 |
  |    same = n,Set(XIVO_CALLOPTIONS=${XIVO_CALLOPTIONS}b(my-pre-dial-handler))           |
  |    same = n,Return()                                                                  |
  |                                                                                       |
  +---------------------------------------------------------------------------------------+
  | New dialplan starting from Jabbah, compatible with ``res_pjsip`` (and ``chan_sip``)   |
  +---------------------------------------------------------------------------------------+
  | .. code-block:: ini                                                                   |
  |                                                                                       |
  |    [my-pre-dial-handler]                                                              |
  |    exten = s,1,NoOp()                                                                 |
  |    same = n,...                                                                       |
  |    same = n,Gosub(xivo-user-predial,s,1)                                              |
  |    same = Return()                                                                    |
  |                                                                                       |
  |    [my-subr]                                                                          |
  |    exten = s,1,NoOp()                                                                 |
  |    same = n,Set(XIVO_CALLOPTIONS=${XIVO_CALLOPTIONS}b(my-pre-dial-handler))           |
  |    same = n,Return()                                                                  |
  |                                                                                       |
  +---------------------------------------------------------------------------------------+

The following functions are availables :

- ``add_header`` to add your header to the upcoming ``Dial``, which takes the following arg:

  - arg1: the name of the SIP Header to add, with quotes
  - arg2: the value of the SIP Header to add, with quotes

+---------------------------------------------------------------------------------------+
| Adding a SIP Header                                                                   |
+=======================================================================================+
| Old dialplan                                                                          |
+---------------------------------------------------------------------------------------+
| .. code-block:: ini                                                                   |
|                                                                                       |
|   same = n,SIPAddHeader(X-My-Context: ${XIVO_BASE_CONTEXT})                           |
|                                                                                       |
+---------------------------------------------------------------------------------------+
| New dialplan starting from Izar, compatible with ``res_pjsip`` (and ``chan_sip``)     |
+---------------------------------------------------------------------------------------+
| .. code-block:: ini                                                                   |
|                                                                                       |
|   same = n,Gosub(xivo_header_mgr,add_header,1("X-My-Context","${XIVO_BASE_CONTEXT}")) |
|                                                                                       |
+---------------------------------------------------------------------------------------+

.. note:: If you need to add a piece of dialplan ending with your own call to the ``Dial``
  application, don't forget to add the call to the header manager in the ``b()`` predial handler,
  even if you did not add any header during your custom subroutine.

  For example :

  .. code-block:: ini
    
    same = n,Dial(PJSIP/qwerty-006a,,b(xivo_header_mgr^set_headers_on_channel^1))

.. _PJSIP_HEADER: https://wiki.asterisk.org/wiki/display/AST/Asterisk+18+Function_PJSIP_HEADER


Retrieving peer information
---------------------------

With ``chan_sip`` you could use the dialplan function `SIPPEER <https://wiki.asterisk.org/wiki/display/AST/Asterisk+18+Function_SIPPEER>`_
to retrieve the ip address of the peer or stuff like that.

Dependeing on the use case, a replacement can be found with:

* dialplan function `CHANNEL <https://wiki.asterisk.org/wiki/display/AST/Asterisk+18+Function_CHANNEL>`_: for example ``CHANNEL(pjsip,remote_addr)`` will retrieve the peer IP of the current channel
* or with dialplan functions `PJSIP_AOR <https://wiki.asterisk.org/wiki/display/AST/Asterisk+18+Function_PJSIP_AOR>`_ and/or `PJSIP_CONTACT <https://wiki.asterisk.org/wiki/display/AST/Asterisk+18+Function_PJSIP_CONTACT>`_. For example, the following will give you the registration status of peer ``abcd``:

  .. code-block:: ini

    ${PJSIP_CONTACT(${PJSIP_AOR(abcd,contact)},status)}

References
==========

* https://wiki.asterisk.org/wiki/display/AST/Migrating+from+chan_sip+to+res_pjsip
* https://wiki.asterisk.org/wiki/display/AST/PJSIP+Configuration+Sections+and+Relationships

