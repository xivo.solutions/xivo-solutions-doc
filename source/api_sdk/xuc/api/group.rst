.. _group_api:

*********
Group API
*********

This API is dedicated to manipulate groups fro UC Assistant.

Groups Events
=============

UserGroups
----------
* Handler on : `Cti.MessageType.USERGROUPS`

The handler is executed when the user logged in the UC Assistant.
The message contains a list of groups the user belong to.

::

    {
        "msgType":"UserGroups",
        "ctiMessage":[
            {"groupId":25,"groupName":"admin_team","groupNumber":"2300","membershipStatus":"Available"},
            {"groupId":23,"groupName":"support_team","groupNumber":"2100","membershipStatus":"Available"}
        ]
    }

UserGroupUpdate
---------------
* Handler on :  `Cti.MessageType.USERGROUPUPDATE`

The handler is executed when a group the user belong to is either edited / deleted / created.

::

    {
        "msgType":"UserGroupUpdate",
        "ctiMessage":{
            "groupId":25,"groupName":"admin_team","groupNumber":"2311","membershipStatus":"Available"
        }
    }

.. membership_status:

Groups Methods
==============

The following methods allow you to manage the state of users within call distribution groups.

Cti.pauseUserGroup(groupId)
---------------------------

This command put user in the group in 'Paused' state, meaning he will not receive call from this group anymore.

Cti.unpauseUserGroup(groupId)
-----------------------------

This command put user in the group in 'Available' state, meaning he will start receiving call from this group again.



MembershipStatus
================

User membership in group events indicate if the user is able to receive calls from the group or not.

The possible values are :

- ``Available``: The user is connected to the group and can receive calls.
- ``Exited``: The user is no longer member of the group.
