.. _ivr-editor:

***********************
IVR Editor :badge:`PRO`
***********************

IVR is a new feature introduced in the Helios LTS.

.. important:: This is an enterprise version feature that is not included in XiVO and is not freely available.
    To enable it, please contact `XiVO team <https://www.xivo.solutions/contact/>`_.


.. contents:: :local:


Introduction
============

   *Interactive voice response (IVR) is a technology that allows a computer to interact with humans
   through the use of voice and DTMF tones input via keypad. In telecommunications, IVR allows
   customers to interact with a company’s host system via a telephone keypad or by speech recognition,
   after which they can service their own inquiries by following the IVR dialogue.*

   -- Wikipedia

The graphical editor allows users to design and configure parts of the dialplan through a visual interface. 
This approach makes it easier to build call flows by using drag-and-drop elements such as menus, call forwarding, and voicemail directly in the editor. 
By avoiding the need for manual configuration or scripting, the editor simplifies the creation and management of the dialplan. 
It also enables quick updates and testing of different scenarios, making it more accessible for administrators to implement changes and maintain the interactive voice response (IVR) system.


IVR Installation & Upgrade
==========================

.. toctree::
   :maxdepth: 1

   install
   

.. _ivr_features:

Features
========

* Create a edit part of dialplan (IVR) in graphical editor.
* Use created IVRs as an incall or a schedule destination
* Send call to queue or other defined dialplan object after IVR
* Upload and manage IVR specific audio files (voice prompts)

.. _ivr_usersguide:

Users' Guide
============

IVR management
--------------

Navigate to :menuselection:`Services -> IPBX` in the Call Management section and select the :menuselection:`IVR`. 
The page will display a list of existing IVRs. 
Each IVR entry includes buttons that allow you to edit the dialplan metadata, modify the dialplan, or delete the dialplan.

Graphical IVR
-------------

The grafical IVR consists of nodes that represent dialplan actions and the connections between them. 
Each node type includes an entry point (a large circle at the top of the node rectangle) that can accept multiple connection lines,
except for the Start node, which does not have an entry point. 
Depending on the type of node, there may be zero or more exit points (small circles at the bottom of the node rectangle). 
The specific exit point used is determined at runtime based on the outcome of the node's action.

In the editor, you can:

* Add a node to the editor pane by selecting the desired node type from the left menu.
* Delete a node by clicking the red cross on the node.
* Configure node parameters by clicking the pencil icon in the top-left corner of the node and filling out the form.
* Connect nodes by dragging the exit point of one node to the entry point of another (or the same) node.
* Remove a connection line by clicking the rectangle with the minus sign on the line.

Node types
----------

Start Node
^^^^^^^^^^
* **Function**: Marks the beginning of the IVR flow. It is created automatically when an IVR is set up and cannot be manually added.
* **Parameters**: This node type does not require any configuration.
* **Screenshot**:

  .. figure:: images/start.png
     :alt: Start Node
     :align: center

Update Node Name
^^^^^^^^^^^^^^^^
* **Function**: Allows you to change the name of a node by double-clicking on the text (unset or orange text).
* **Parameters**:

  * :guilabel:`Name`: The name to be assigned to the node.
* **Screenshot**:

  .. figure:: images/name.png
     :alt: Update Node Name
     :align: center

Menu Node
^^^^^^^^^
* **Function**: Presents a menu choice to the caller. Each key (0-9, \*, #) can be configured to direct the call to different nodes or actions.
* **Parameters**:

  * :guilabel:`Repeat`: The number of times the message will be repeated if no response is given.
  * :guilabel:`Timeout`: The maximum duration to wait before considering that no response was given.
  * :guilabel:`Menu voiceprompt`: The audio file used for the menu announcement. (e.g., ``ivr-example-choices``).
  * :guilabel:`No choice or invalid voiceprompt`: The message played if no choice is made or the choice is invalid. (e.g., ``ivr-example-invalid-choice``).
  * :guilabel:`Outdated attempt voiceprompt`: The audio file played when all response attempts have failed. (e.g., ``ivr-example-error``).
* **Screenshot**:

  .. figure:: images/menu.png
     :alt: Menu Node
     :align: center

User Node
^^^^^^^^^
* **Function**: Allows you to direct the call to a specific user.
* **Parameters**:

  * :guilabel:`First and Last Name`: The full name of the user. You must enter at least three characters to trigger a dropdown list that helps with the selection. Once the name is selected, the **Internal Number** field is automatically filled.
  * :guilabel:`Internal Number`: The user's internal extension number. You must enter at least three digits to activate the dropdown suggestions. Once the number is chosen, the **First and Last Name** field is automatically completed.
* **Screenshot**:

  .. figure:: images/user.png
     :alt: User Node
     :align: center

Group Node
^^^^^^^^^^
* **Function**: Directs the call to a group of users.
* **Parameters**:

  * :guilabel:`Group name`: The name of the user group.
* **Screenshot**:

  .. figure:: images/group.png
     :alt: Group Node
     :align: center

Queue Node
^^^^^^^^^^
* **Function**: Sends the call to a specific queue, usually for support or a technical service.
* **Parameters**:

  * :guilabel:`Name`: Select the name of the queue.
* **Screenshot**:

  .. figure:: images/queue.png
     :alt: Queue Node
     :align: center

Voicemail Node
^^^^^^^^^^^^^^
* **Function**: Allows you to transfer the call to a specific voicemail.
* **Parameters**:

  * :guilabel:`Full name`: The name of the voicemail recipient. Enter at least three characters to display a dropdown list of suggestions. Once the name is selected, the **Voicemail number** field is automatically filled.
  * :guilabel:`Voicemail number`: The number associated with the voicemail. You must enter at least three digits to activate the dropdown suggestions. Once the number is chosen, the **Full name** field is automatically completed.
* **Screenshot**:

  .. figure:: images/voicemail.png
     :alt: Voicemail Node
     :align: center

Playback Node
^^^^^^^^^^^^^
* **Function**: Plays an audio file for the caller. Typically used for welcome messages or instructions.
* **Parameters**:

  * :guilabel:`Voice prompt file`: Select the audio file to be played (e.g., ``ivr-example-welcome-sound``).
* **Screenshot**:

  .. figure:: images/playback.png
     :alt: Playback Node
     :align: center

Hangup Node
^^^^^^^^^^^
* **Function**: Ends the current call. Used to terminate an IVR flow.
* **Parameters**: No specific parameters need to be configured.
* **Screenshot**:

  .. figure:: images/hangup.png
     :alt: Hangup Node
     :align: center

Goto Node
^^^^^^^^^
* **Function**: Allows you to redirect the call to another context or a specific extension.
* **Parameters**:

  * :guilabel:`Context`: The target context.
  * :guilabel:`Extension`: The extension to which the call should be directed.
* **Screenshot**:

  .. figure:: images/goto.png
     :alt: Goto Node
     :align: center

Use Case: Minimal Graphic IVR
=============================

Configuration of a graphic IVR linked to a schedule and an incoming call.

Flowchart
---------

.. figure:: images/ivr-schema-minimal.png

Configure the Graphic IVR
-------------------------

First, you need to create a graphic IVR.  
In our example, the graphic IVR is named ``Minimal IVR``.

.. figure:: images/ivr-create.png

Once the IVR has been created, you can configure your graphic IVR.

.. figure:: images/ivr-minimal.png
   :align: center

Set Up Graphic IVR for External Dialing
---------------------------------------

To allow calls to the graphic IVR from an external number,  
you need to create an incoming call route and redirect the call to the graphic IVR:

.. figure:: images/ivr-create-incall.png

Create a Schedule
-----------------

First, create your schedule (1) from the menu :menuselection:`Call Management --> Schedules`.  
In the "General" tab, provide a name (3) for your schedule, configure the open hours (4), and select  
the audio file to be played when the company is closed.

In the "Closed Hours" tab (6), configure all special closed days (7) and select the audio file that  
will inform the caller that the company is exceptionally closed.

The IVR script will now only be available during working hours.

.. figure:: images/ivr-schedule.png

Assign the Schedule to the Incoming Call Route
----------------------------------------------

Return to editing your incoming call route (:menuselection:`Call Management --> Incoming Calls`)  
and assign the newly created schedule in the "Schedules" tab.

.. figure:: images/ivr-schedule-assign.png

Use Case: Graphic IVR with Submenu
==================================

Flowchart
---------

.. figure:: images/ivr-schema-submenu.png

Configure the Graphic IVR with a Submenu
----------------------------------------

Once the IVR has been created, you can configure your graphic IVR with a submenu.

.. figure:: images/ivr-submenu.png
   :align: center