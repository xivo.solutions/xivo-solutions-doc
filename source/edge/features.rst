*************
Edge Features
*************

.. contents:: :local:

Proxy Web Features
==================

The Web proxy is configured to:

* proxify only the needed route towards the XiVO CC
* rate-limit the call to the application
* verify that calls to ``/xuc/api/2.0/cti`` endpoint (CTI WS) contains a token in the right
  format in the argument
* verify that calls to ``/wssip`` and ``/wssip-MDSNAME`` endpoint (SIP WS) contains a token in the argument

.. _edge_feature_proxy_turns_on_port_443:

Expose TURNS server on port 443
-------------------------------

(*Since 2025.02.00*)

.. note:: This applies only to the **1 Server Installation**

.. important:: Pre-requisite: have two different FQDN resolving to the ``EDGE_HOST_IP``: one for the web the other for the turn.

In Edge deployment the TURN server is listening for TURN and TURNS request on port **3478**.
In some environement this port may not be accessible: in this case the TURN server can not be used as a Relay.

To mitigate this issue it is possible to configure the Edge server to make TURNS available on port 443.

How to activate this feature
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. important:: Pre-requisite: have two different FQDN resolving to the ``EDGE_HOST_IP``:

   * a ``EDGE_FQDN`` (e.g. edge.mycorp.com) resolving to ``EDGE_HOST_IP``
   * **and** another ``TURN_FQDN`` (e.g. edge-turn.mycorp.com) resolving *also* to ``EDGE_HOST_IP``

#. **Edit** the :file:`/etc/docker/edge/.env` file adding the following variables:

   .. code-block::
      :emphasize-lines: 1,2

      EDGE_NGINX_TURN_PROXY=true
      TURN_FQDN=<FQDN for TURN> # e.g. edge-turn.mycorp.com

#. Relaunch Edge services - see :ref:`edge_administration_launch`


How to deactivate this feature
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. **Remove** the ``EDGE_NGINX_TURN_PROXY`` and ``TURN_FQDN`` from the :file:`/etc/docker/edge/.env` file.

   .. code-block:: bash

      sed -i '/^EDGE_NGINX_TURN_PROXY/d' /etc/docker/edge/.env
      sed -i '/^TURN_FQDN/d' /etc/docker/edge/.env

#. Relaunch Edge services - see :ref:`edge_administration_launch`

.. _edge_feature_proxy_web_extend_configuration:

Extend the Configuration
------------------------

(*Since Naos.03*)

It is possible to extend the Nginx configuration.
Our Edge Nginx is split under 2 server_:

* one on port 80,
* one on port 443 with SSL

It is possible to extend each of these server_ section. It is mainly useful to add some location_.


#. Create a directory on your Egde Web Proxy server:

   .. code-block:: bash

     mkdir -p /etc/docker/nginx/extra-conf.d/{http,https}/
#. **Edit** the Edge Nginx compose file :file:`/etc/docker/edge/nginx-edge.yml` to add the following volume:

   .. code-block:: yaml
      :emphasize-lines: 6

      services:
        nginx:
        [...]
          volumes:
          - /etc/docker/ssl:/etc/nginx/ssl
          - /etc/docker/nginx/extra-conf.d/:/etc/nginx/templates/extra-conf.d/
          logging:
        [...]
#. Now add you can extend one of the server configuration section. For example:

   * create a file :file:`/etc/docker/nginx/extra-conf.d/https/xivocc-deny-callback-api.conf.template`
   * with the folowing content:

     .. code-block:: ini

        location ~ ^/xuc/api/2.0/callback_lists {
          deny all;
        }
   * this will deny the call to callback_lists API

.. _server: https://nginx.org/en/docs/http/ngx_http_core_module.html#server
.. _location: https://nginx.org/en/docs/http/ngx_http_core_module.html#location



.. _edge_feature_proxy_web_nb_connection:

Number of Connections
---------------------

*Since Maia.11*

The number of connections Nginx can handle is given by the formula::

  max connections = worker_processes * worker_connections

The Edge Nginx container:

* tunes the ``worker_processes`` variable at startup (this can be changed with the ``NGINX_ENTRYPOINT_WORKER_PROCESSES_AUTOTUNE`` to false)
* sets the ``worker_connections`` by default to 20000 - it means the Proxy Web should be able to handle ~5000 clients per worker process (given a client opens 2 WebSocket which are proxified, a client uses 4 connections)

  .. note:: It can be overriden wih ``NGINX_WORKER_CONNECTIONS`` env var

* displays in startup log the ``worker_processes`` and ``worker_connections`` configured
  and an estimation of the maximum number of connections given these figures


References:

* https://nginx.org/en/docs/ngx_core_module.html#worker_processes
* https://nginx.org/en/docs/ngx_core_module.html#worker_connections

Certificates and OCSP
---------------------

By default OCSP_ is enabled.
For OCSP to work the Intermediate Certificate must be correctly installed. Normally this should be ok if you
did install the *fullchain* certificate as specified in the :ref:`edge_config_sslcert` config section.

If it is not the case one could deactivate OCSP for the web proxy by adding the following in the :file:`env` file:

.. code-block:: ini

  OCSP_ENABLE=off

.. _OCSP: https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol

.. _edge_nginx_rate_limiting:

Rate limiting
-------------

Simplified explanation on how the nginx rate limit module works :

.. figure:: images/edge_nginx_ratelimiting.png
   :alt: edge_nginx_ratelimiting

Rate limit does not apply to *Private IP Address* (RFC1918)

Rate limiting is configured in three zones and request limits are
applied by client ip :

1. **webapp**

   -  **30 req/s, burst of 20 req**

      -  /ccagent
      -  /ucassistant
      -  /switchboard

   -  **30 req/s, burst of 50 req (env variable), nodelay**

      -  /assets
      -  /config
      -  /video/css
      -  /video/images
      -  /video/lang
      -  /video/libs
      -  /video/sounds
      -  /pwa-worker.js
      -  /static/offline.html

2. **apis**

   -  **30 req/s, burst of 10 req**

      -  /xuc
      -  /version

   -  **30 req/s, burst of 10 req, nodelay**

      -  /video/external_api.js

3. **auth**

   -  **20 req/m, burst of 12 req, nodelay**

      -  /wssip
      -  /xuc/api/2.0/auth/login
      -  /xuc/api/2.0/cti
      -  /invitation/video
      -  /meetingrooms/token/validate/

4. **dapp**

   - **20 req/m, burst of 15 req, nodelay**

      -  /install/win64
      -  /updates/win64
      -  /updates/debian

5. **video**

   - **20 req/s, burst of 12 req, nodelay**

      -  /video/
      -  /video/xmpp-websocket
      -  /video/colibri-ws/

You can tweak the rate limit burst of the asset part by changing the value of the ``ASSETS_BURST`` variable.


Token format validation
-----------------------

The token format is checked with a regex.
It checks the token contains 3 segments between 15 and 120 characters separated with dots.


TURN Server Features
====================

.. _edge_feature_turn_turns_on_port_443:

Use TURN server on port 443
---------------------------

(*Since 2025.02*)

See :ref:`edge_feature_proxy_turns_on_port_443`


TURN Credentials
----------------

To be able to use the TURN server you must have valid credentials.
Credentials are given by the xucserver to the WebRTC client.

These credentials are generated by the xucserver and validated/verified
by the TURN server via a shared secret defined:

* in xuc via the ``TURN_SERVER_SECRET`` variable
* in TURN Server via the ``static-auth-secret`` parameter


By default only TURN is activated for both asterisk and WebRTC client.
This can be changed via the ``TURN_SERVER_ENABLE`` and ``STUN_SERVER_ENABLE`` variables to be put in the ``custome.env`` of the XiVO CC

::

    ...
    TURN_SERVER_ENABLE=false
    STUN_SERVER_ENABLE=true

TURN Credentials lifetime
-------------------------

These credentials are valid for a default lifetime of **3600s**. The
WebRTC client renew the credential every TTL/2.

The TTL can be changed via the ``TURN_SERVER_CREDENTIAL_TTL`` xuc
environment variable. To change the credential TTL to 2hours add the
following in the XiVOCC ``custom.env`` file:

::

    ...
    TURN_SERVER_CREDENTIAL_TTL=7200


Optimize ICE (STUN/TURN) negotiation time
-----------------------------------------

There are two places where the ICE mechanism can take some time:

1. Before emitting the call
2. When call is answered

1. Before emitting the call
^^^^^^^^^^^^^^^^^^^^^^^^^^^

When A wants to call B, A will start - before sending the SIP INVITE, by
gathering what is called the "candidate". This is A's host IP addresses
and also what the STUN/TURN server will give him.

The more host IP addresses A has, the longer the STUN/TURN gathering
will take. Not to say that it also depends on the fact that these IP
addresses can reach the STUN/TURN server.

*  On the UC Application side and Mobile application the candidate
   gathering process is limited to **500 milliseconds**. See :ref:`edge_config_xivocc_config` for tweaking it.
*  On asterisk side this can be enhanced by setting a stun and ice
   blacklist rule. This can be done by adding the following lines in a
   file in :file:`/etc/asterisk/rtp.d/` directory (for example
   :file:`/etc/asterisk/rtp.d/02-ice-optimization.conf`):

   .. code-block:: ini

     stun_deny = 0.0.0.0/0
     stun_permit = 10.32.4.0/24

     ice_deny = 0.0.0.0/0
     ice_permit = 10.32.4.0/24


Where `10.32.4.0/24` is the network to which belongs the interface which can access the STUN/TURN server.

You need to reload rtp: `asterisk -rx 'module reload res_rtp_asterisk.so'`

.. warning::
    - Please read carefully : asterisk documentation https://github.com/asterisk/asterisk/blob/18/configs/samples/rtp.conf.sample
    - Beware that a mis-configuration there can prevent your WebRTC user to make any call


2. When call is answered
^^^^^^^^^^^^^^^^^^^^^^^^

If A calls B, when B answers, A and B will start to negotiate which RTP candidate they will use to communicate.
The longer the candidate list for A and B the greater the number of possibility and connectivity check to do.


* On the UC Application side there is not much one can do except to shutdown some network interfaces.
  Nevertheless, the candidate gathering process is limited to **500 milliseconds** and can be overriden to increase or reduce this timeframe. See :ref:`edge_config_xivocc_config`
* On asterisk side this can be enhanced by setting a stun and ice blacklist rule.
  This can be done by adding the following lines in a file in :file:`/etc/asterisk/rtp.d/` directory
  (for example :file:`/etc/asterisk/rtp.d/02-ice-optimization.conf`):

  .. code-block:: ini

    stun_deny = 0.0.0.0/0
    stun_permit = 10.32.4.0/24

    ice_deny = 0.0.0.0/0
    ice_permit = 10.32.4.0/24

Where ``10.32.4.0/24`` is the network to which belongs the interface
which can access the STUN/TURN server.

You need to reload rtp:
``asterisk -rx 'module reload res_rtp_asterisk.so'``

.. warning::
  - Please read carefully : asterisk documentation https://github.com/asterisk/asterisk/blob/18/configs/samples/rtp.conf.sample
  - Beware that a mis-configuration there can prevent your WebRTC user to make any call
