.. _directmedia_sipprovider_rfc2833_configuration:

******************************************************
Activate Directmedia with SIP Provider & DTMF RFC 2833
******************************************************

.. important:: This page describes how to enable Directmedia on your XiVO PBX in **the specific following context**:

    * with a SIP Provider authorizing directmedia (or requiring it),
    * and with this SIP Provider using DTMF according to RFC 2833

    Other use cases are not covered here.


Introduction
============

Directmedia is an Asterisk feature to optimize network streams.
By default, when asterisk establishes a call between two phones, it establishes the media stream (voice or video RTP stream) via
itself. Then, if A calls B, the media stream goes from A to asterisk and then from asterisk to B::

  Without directmedia:

                             +----------+
     A <---- SIP & RTP ----> |   XiVO   | <---- SIP & RTP ----> B
                             +----------+

Enabling Directmedia make media streams to flow directly between A and B::

  With directmedia activated:

                             +----------+
     A <------- SIP -------> |   XiVO   | <------- SIP -------> B
                             +----------+
     ^                                                          ^
     ^_________________________  RTP  __________________________^

This is particularly useful when your XiVO PBX and phones are not located on the same site.
In this case, activating Directmedia could dramatically save network bandwidth.


Prerequisites
=============

* As written in the note above we only cover the case where your SIP Provider will accept media stream directly between your
  endpoints and its media gateways.
* Also it describes how to configure your system if your SIP Provider accepts DTMF in RFC 2833.
* And you **must** ensure that network routing is working between your different endpoints - as enabling directmedia means that media stream will
  flow directly between endpoints (phones, gateways, operators etc.).
* Note also that in this context network QoS should be taken into account with great care, but is outside the scope of this
  page.


Configuration
=============

Directmedia activation
----------------------

First of all, enable directmedia on your XiVO PBX:

#. Go to :menuselection:`Services --> IPBX --> General settings --> SIP Protocol --> Default`
#. Set :guilabel:`Redirect media stream` to **Not behind NAT**
#. Set :guilabel:`DTMF` to **RFC2833**


Device configuration
--------------------

Device configuration must be changed to activate the RFC 2833 DTMF mode:

#. Go to :menuselection:`Configuration --> Provisioning --> Template device`
#. Edit the :menuselection:`Default template device`
#. and set :guilabel:`DTMF` to **RTP-out-of-band**
#. **This requires a synchronization of all Devices** (see :ref:`synchronize-device`)


SIP Provider Trunk Configuration
--------------------------------

Verify that your SIP Provider trunk configuration has also directmedia activated:

#. Edit your SIP Provider trunk,
#. In tab :menuselection:`Advanced` set :guilabel:`Redirect media streams` to **Not behind NAT**
#. In tab :menuselection:`Signalling` set :guilabel:`DTMF` to **RFC2833**


Other SIP Trunks
----------------

If you have other SIP trunks on your installation, you should verify that the DTMF mode is according to what the endpoint support.
And, as it is not covered in this guide, we recommend that you deactivate directmedia:

#. Edit the SIP trunk,
#. In tab :menuselection:`Advanced` set :guilabel:`Redirect media streams` to **No**
#. In tab :menuselection:`Signalling` set :guilabel:`DTMF` to the one supported by the endpoint.


User configuration
------------------

For directmedia to work with DTMF set in RFC 2833 mode you **must** deactivate the following options for users:

#. Go to :menuselection:`Services --> IPBX --> IPBX Settings --> Users`,
#. edit each user - **except Switchboard user**,
#. go to tab :menuselection:`Services`
#. and un-check all following options:

  * :guilabel:`Enable call transfer`
  * :guilabel:`Enable online call recording`


Groups configuration
--------------------

For directmedia to work with DTMF set in RFC 2833 mode you **must** deactivate the following options for groups:

#. Go to :menuselection:`Services --> IPBX --> IPBX Settings --> Groups`,
#. edit each group,
#. go to tab :menuselection:`Application`
#. and un-check all following options:

  * :guilabel:`Allow called one to transfer the caller`
  * :guilabel:`Allow caller to transfer the call`
  * :guilabel:`Allow called on to record the call`
  * :guilabel:`Allow caller to record the call`


Queues configuration
--------------------

For directmedia to work with DTMF set in RFC 2833 mode you **must** deactivate the following options for queues:

#. Go to :menuselection:`Services --> Call Center --> Configuration --> Queues`,
#. edit each queue - **except the Switchboard queue**,
#. go to menu :menuselection:`Application`
#. and un-check all following options:

  * :guilabel:`Allow callee to hang up the call`
  * :guilabel:`Allow caller to hang up the call`
  * :guilabel:`Allow callee to transfer the call`
  * :guilabel:`Allow caller to transfer the call`
  * :guilabel:`Allow callee to record the call`
  * :guilabel:`Allow caller to record the call`

Conclusion
==========

After having followed the above configuration steps, your XiVO will be configured for directmedia with a SIP Provider using DTMF
in RFC 2833 mode.


Limitations
-----------

This configuration is not compatible with:

* Switchboard: calls towards Switchboard will be without directmedia. Directmedia will be activated on the call after having
  been transfered (as the legacy transfer method must be used for Switchboard CTI application)
* Jitterbuffer: jitterbuffer must be disabled on your XiVO PBX.
* Recording: if the call recording is activated for calls, it will disable Directmedia on these calls.
* ChanSpy: listening to an agent (see CCManager application documentation) will disable temporarily the directmedia.
  When listening is stopped, media flow will be re-established directly between agent and caller.
