.. _mobile_application_users_guide:

******************
Mobile Application 
******************

**Contents:**

.. contents:: :local:

.. _environment_and_use_cases:

Environment and use cases 
=========================

The XiVO mobile application comes in addition to the UC Assistant. It allows you to access the services deployed on your XiVO environment via your smartphone. You will need a Wifi or 4/5G connection.

.. note:: The mobile application can be used in WebRTC mode only.

The use of the application can be  :

    • Exclusive, i.e. it is your unique Xivo environment
    • Shared, i.e. a hybrid approach with your UC Assistant. Several combinations of call processing are then possible.

 You can do the following actions: 
    • Call
    • Receive a call
    • Transfer a call
    • Call via your favorites
    • Call by searching in the company directory
    • Call by searching in your local cell phone directory
    • Forwarding management
    • Call management
    • Visibility on the presence of your colleagues
    • Access to voice messaging
    • Access to the history
    • Pop-up notification for missed calls
    • Pop-up message notification on the voice mail.

The following services are not currently available : 
    • Video conferencing
    • Chat

These features are in our roadmap.


.. _download:

Download
=========

.. figure:: images/download-mobile-app.png
   :scale: 80%


Google Play Store 
------------------

The Xivo - Xivo mobile application is available on the Android Play Store. You need to search for the "XiVO" application.
Once on the application you can tap *install* and then *open the application*.

.. figure:: images/mobile-app-play-store.png
   :scale: 80%

App Store
---------

The Xivo - Xivo mobile application will be available on the IOS App Store.
You need to search for the application "XiVO".

.. _first_launch:

First launch
============

When you open the application for the first time, it will ask for different authorizations :
* Mandatory authorizations, for the application to function properly
* An optional authorization, to allow access to your phone contacts.

  * List of requested authorizations : 
    • Stop battery optimization
    • View in the foreground of the application
    • Microphone
    • Phone account to make calls
    • Local phone book contacts (Optional)

.. figure:: images/first-use1.png
   :scale: 80%

.. figure:: images/first-use2.png
   :scale: 80%

.. figure:: images/first-use3.png
   :scale: 80%

.. figure:: images/first-use4.png
   :scale: 80%

.. figure:: images/first-use5.png
   :scale: 80%

.. figure:: images/first-use6.png
   :scale: 80%

.. _first_connection:

First connection
================

.. figure:: images/first-connection.png
   :scale: 80%

When you first connect to the application, a connection page will be displayed with the following fields : 
  * the address of the telephony server,
  * your login,
  * your password

.. figure:: images/first-connection2.png
   :scale: 80%

Where to find the server address:

* On your Web browser

If you use your web browser to access Xivo, you will find the information directly in the address bar.

.. figure:: images/url-server-app.png
   :scale: 80%

* On your Desktop Assistant

THe server address is in the parameters, on the upper right corner of your desktop assistant. Once in the settings, use the information in "application server".

.. figure:: images/first-connection-desktop.png
   :scale: 80%

If you use only the app for *Xivo connectivité*, please ask your IT manager for the url.

About the authentication fields:
You must use the username and password that you usually use when logging in.

When you first login, a notification will be displayed in your UC Assistant. It shows you that you can choose, via the call management, on which devices you want to receive calls. See gif below :

.. figure:: images/mobile_notification_en.gif
   :scale: 80%

.. _home page:

Home page
=========

Once you are logged in, you will be taken to the home page which is a dial pad.
From this page you can access :
* A dial pad to initiate a call via a number
* Your Favorites
* Your History
* Your voice mail when you have one
* A search bar
* Access to the configuration

.. figure:: images/home-mobileapp.png
   :scale: 80%
   
   
You can launch a call by typing a number on the keyboard. For example, if I type \*55 to launch a call to the echo test, a call button appears at the bottom of the screen.


.. _favorites:

Favorites
=========

.. figure:: images/mobile-app-fav.png
   :scale: 80%

You can access your favorites through the button located at the bottom left of the mobile application. 
Once in your favorites, you can launch a call by pressing directly on the handset on the right of the chosen contact.
You will also have a view on the status of your colleagues.

.. _history:

History
=======

.. figure:: images/history-mobileapp.png
   :scale: 80%

You can access your call history through the button on the bottom right of the mobile application.
In the history you will have the possibility to launch a call by pressing directly on the handset.
You'll also be able to see the status of the other users.

.. _mobile_search:

Mobile search 
=============

.. figure:: images/search-mobileapp.png
   :scale: 80%

You can find the search bar on the top right of the mobile application on all of the following menus: Phone, History, Favorites.

You will be able to search your personal XiVO directory, the company directory and also the local directory of your phone.


.. _incoming call:

Incoming call
=============

.. figure:: images/incoming-call-mobileapp.png
   :scale: 80%

The reception of a call is done via push notification - this notification wapes up the app).

IOS  
---

When receiving a call on IOS, you can switch directly to the mobile application via the XiVO button by unlocking your mobile.

.. figure:: images/incoming-ios-mobileapp.png
   :scale: 80%

.. figure:: images/incoming-ios-mobileapp2.png
   :scale: 80%

Android
-------

When receiving a call on Android, the call management will be accessible on the mobile application when you have unlocked your mobile.

.. figure:: images/incoming-android.png
   :scale: 80%


.. figure:: images/incoming-android2.png
   :scale: 80%


In call display :

After picking up the phone you can pause a call, mute the microphone, activate speakers. The “+” button allows to call another number.

.. _outgoing_call:

Outgoing call
=============

In some cases, you might get "Your call is in transit, thank you for your patience" before getting the screen "your call is ringing".
Once the person you are calling has picked up the phone you will be taken to the call management page.

.. figure:: images/outgoing-mobileapp.png
   :scale: 80%

.. _managing_a_call:

Managing a call
===============

When you are on a call, whether it’s an incoming or outgoing call, you get the call management options. 

 The following actions are available :

.. figure:: images/managing-call1.png
   :scale: 80%

.. figure:: images/managing-call2.png
   :scale: 80%

.. figure:: images/managing-call3.png
   :scale: 80%


Call transfer  
-------------

.. figure:: images/mobileapp-transfer.png
   :scale: 80%

You can transfer a call while being on a call.
There are two ways to do it:
* Pressing the "+" button allows you to go directly to the Favorites and select the person you want to reach for your transfer. Once you have selected your favorite, the call will be launched and the person you were talking to will be put on hold.

* You can access the pages : 

   * Favorites
   * History
   * Search

From one of these pages, you can launch a second call and put on hold the person with whom you were in a conversation.

.. figure:: images/mobileapp-transfer2.png
   :scale: 80%

Put on hold
-----------

When you receive a call while being already on a call, you can put your current call on hold. In this context, the person you put on hold will hear a music on hold. You can then resume the call via the play button.

.. _notifications:

Notifications
=============

Notifications are integrated in the mobile application.
You will receive these notifications on your mobile application when you have a missed call.

.. figure:: images/mobileapp-notifications.png
   :scale: 80%

.. _call_management:

Call management
===============

Device management  
-----------------

.. figure:: images/callmanagement-mobileapp.png
   :scale: 80%

During the installation and the first connection to the mobile application, you must have noticed a change in the call management of your UC assistant.

You can, on your UCAssistant select the ringing device:

.. figure:: images/device-management-mobileapp.png
   :scale: 80%


This allows you to select either your UC Assistant or the mobile application or both.

Documentation link below : 
https://documentation.xivo.solutions/en/2022.10/usersguide/uc_assistant/index.html#using-web-and-mobile-applications-together

This management capabilities will also be available in the app.

Please find some examples below: 

.. figure:: images/mobileapp-device-management2.png
   :scale: 80%

When selecting the mobile application, you will see a mobile icon in the top right of the application.

.. figure:: images/mobileapp-device-management3.png
   :scale: 80%

When selecting the mobile application and the web browser, you will see a mobile and a phone icon in the top right of the application.

.. figure:: images/mobileapp-device-management4.png
   :scale: 80%

When selecting the web browser, you will see a phone icon on the top right of the application.

Do not disturb 
--------------

.. figure:: images/do-not-disturb.png
   :scale: 80%

When activating the "do not disturb" mode, you will see a "do not disturb" sign in the top right corner of the call management.

Forward all calls
-----------------

.. figure:: images/forward-all-calls-mobileapp.png
   :scale: 80%

When activating the "forward all calls" you will see an arrow on the devices on the top right corner of the call management.

Forward calls on no answer  
--------------------------

.. figure:: images/forward-calls-on-no-answer.png
   :scale: 80%

When activating the mode "forward calls on no answer", you will see a forwarding arrow in the icons located on the top right corner of the page.

.. _configuration:

Configuration
=============

.. figure:: images/configuration-mobileapp.png
   :scale: 80%

You can access the configuration by pressing the button on the top right corner of the call management.
In this menu you have access to the following actions:
-Remove access to my phone contacts
-Integration of your forwarding number 
-Xivo server address
-Activation of call forwarding

.. _disabling_application:

Disconnecting / disabling application
=====================================

You can disconnect and disable the app by pressing the button at the bottom center of the page.
When you log out, you wont be able to receive calls on your smartphone anymore. In this case, only the UC Assistant -or your browser - will ring if you are connected to it.
