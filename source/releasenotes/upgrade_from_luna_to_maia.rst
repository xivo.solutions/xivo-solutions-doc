**********************
Upgrade Luna to Maia
**********************

.. contents:: :local:


.. warning:: For Maia Debian was upgraded to Debian 12 (Bookworm).

  Therefore:

      * the upgrade to Maia will take longer than usual
      * upgrade from older version than XiVO Izar (2022.05) are not supported (you need first to upgrade
        to a version above or equal to Izar before being able to upgrade to Maia).

    Please read carefully :ref:`Debian 12 (Bookworm) upgrade notes <debian_12_upgrade_notes>` page.

    Note also that upgrade to Debian 12 on MeetingRooms, Edge and XiVO CC is manual (see manual procedure below).

Before Upgrade
==============

On XiVO PBX
-----------

N.A.

On XiVO CC
----------

* The XiVO CC upgrade is now handled by the ``xivocc-upgrade`` script.

  #. You need to install it manually to be able to use it:

     .. code-block:: bash

      xivo-dist xivo-maia
      apt update
      apt install xivocc-upgrade

  #. Then follow the XiVO CC upgrade process as described in :ref:`upgrade_to_lts_version_xcc`


On MDS
------

N.A.

On Edge
-------

* Upgrade system to Debian 12 (Bookworm):

  #. Rewrite the apt preferences for docker-ce

     .. code-block:: bash

      #set docker-ce preferences
      cat > /etc/apt/preferences.d/docker-ce << EOF
      Package: docker-ce*
      Pin: version 5:25.0.5*
      Pin-Priority: 1000

      EOF

  #. Remove docker-compose

     .. code-block:: bash

      rm -f /usr/local/bin/docker-compose

  #. Install docker compose plugin

     .. code-block:: bash

      apt update
      apt install docker-compose-plugin

  #. Update bash alias to launch services:

     .. code-block:: bash

      sed -i 's/docker-compose/docker compose/g' ~/.bashrc
      source ~/.bashrc

  #. and then follow the :ref:`Debian 12 upgrade manual procedure <upgrade_bookworm>`:

On Meeting Rooms
----------------

* Upgrade system to Debian 12 (Bookworm):

  #. Rewrite the apt preferences for docker-ce

     .. code-block:: bash

      #set docker-ce preferences
      cat > /etc/apt/preferences.d/docker-ce << EOF
      Package: docker-ce*
      Pin: version 5:25.0.5*
      Pin-Priority: 1000

      EOF

  #. Remove docker-compose

     .. code-block:: bash

      rm -f /usr/local/bin/docker-compose

  #. Install docker compose plugin

     .. code-block:: bash

      apt update
      apt install docker-compose-plugin

  #. Update bash alias to launch services:

     .. code-block:: bash

      sed -i 's/docker-compose/docker compose/g' ~/.bashrc
      source ~/.bashrc

  #. and then follow the :ref:`Debian 12 upgrade manual procedure <upgrade_bookworm>`:



After Upgrade
=============

On XiVO PBX
-----------

* Upgrade: the upgrade deactivated all apt sources list except those needed for XiVO (xivo, debian, docker, postgres).
  After the upgrade you might need to reactivate some custom sources list you had. **Please make sure that they are compatible with Debian 12**.

* If you customized your directories, upgrade them so they match the new display filter.
  Default directories (xivodir, internal) have been automatically upgraded to match the new Display filter variable.
  Check out :ref:`internal <xivo-directory>` matching the xivo directory (internal xivo users) and :ref:`xivodir <phonebook-directory>` for the phonebook contacts.
  You can reduce the value mapping to what stands as default, and/or update your customization.


On MDS
------

* Upgrade: the upgrade deactivated all apt sources list except those needed for XiVO (xivo, debian, docker, postgres).
  After the upgrade you might need to reactivate some custom sources list you had. **Please make sure that they are compatible with Debian 12**.


On XiVO CC
----------

* Upgrade system to Debian 12 (Bookworm) with the :ref:`following manual procedure <upgrade_bookworm>`:

On Edge
-------

Ensure you correctly replaced the alias edge-dcomp :ref:`as stated in the documentation <edge_upgrade>`.

On Meeting Rooms
----------------

Ensure you correctly replaced the alias meetingrooms-dcomp :ref:`as stated in the documentation <meetingrooms_upgrade>`.

.. _upgrade_bookworm:

Upgrade to Debian12
===================

* These steps are to be done before an upgrade from Luna to Maia on **XiVO CC**, **Edge** and **Meeting Rooms**:

  #. Check GRUB **before upgrading**

     .. code-block:: bash

      install_device=$(debconf-show grub-pc | grep 'grub-pc/install_devices:' | cut -b3- | cut -f2 -d' ' | cut -d',' -f1)
      if [ "$install_device" -a ! -e "$install_device" ]; then
        echo -e "\e[1;31mYou must install GRUB BEFORE upgrading\e[0m"
      fi

     If it's broken you can fix it this way before rechecking

     .. code-block:: bash

      apt update
      apt install grub-pc
      dpkg-reconfigure grub-pc

  #. Check you are in Debian 11 (we prevent upgrade from Debian 10 - see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=993755#62)

     .. code-block:: bash

      if [ "$(cut -d '.' -f 1 /etc/debian_version)" -ne 11 ]; then
        echo -e "\e[1;31mYou are not in Debian 11. Please upgrade first to Debian 11\e[0m"
        echo -e "\e[1;31mSee Helios to Izar Upgrade page.\e[0m"
      fi

  #. Upgrade to Debian12

     .. code-block:: bash

      # Move to bookworm
      sed -i 's/bullseye/bookworm/' /etc/apt/sources.list /etc/apt/sources.list.d/*.list
      apt update

      export DEBIAN_FRONTEND=noninteractive
      export APT_LISTCHANGES_FRONTEND=none
      force_yes="--allow-downgrades --allow-remove-essential --allow-change-held-packages"

      echo "Download packages..."
      apt full-upgrade -d --yes
      echo "Executing full upgrade actions..."
      apt full-upgrade --yes ${force_yes} -o Dpkg::Options::="--force-confnew"
      apt autoremove --yes

  #. Reboot
