.. XiVO-doc Documentation master file.

******************************************************
XiVO Solutions |version| Documentation (Orion Edition)
******************************************************

.. important:: **What's new in this version ?**

   - *To be filled*

   **Deprecations**
      * End of Support for LTS Izar (2022.05).

   See :ref:`orion_release` page for the complete list of **New Features** and **Behavior Changes**.


.. figure:: logo_xivo.png


XiVO_ solutions is a suite of PBX applications based on several free existing components including Asterisk_
and our own developments. This powerful and scalable solution offers a set of features for corporate telephony and call centers to power their business.

You may also have a look at our `development blog <http://xivo-solutions-blog.gitlab.io/>`_ for technical news about the solution

.. _Asterisk: http://www.asterisk.org/
.. _XiVO: https://xivo.solutions/

.. role:: badge
  :class: badge

.. toctree::
   :maxdepth: 2

   introduction/introduction
   getting_started/getting_started
   installation/index
   administrator/index
   ipbx_configuration/administration
   contact_center/contact_center
   edge/index
   meetingrooms/index
   ivr/index
   mobile_application/index
   usersguide/index
   Devices <https://documentation.xivo.solutions/projects/devices/en/latest/>
   api_sdk/api_sdk
   telemetry/index
   contributing/contributing
   releasenotes/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
