.. _asterisk_http_server:

********************
Asterisk HTTP server
********************

Asterisk HTTP server is used for WebRTC and xivo-outcall.

.. warning:: **Security warning:** Asterisk HTTP server is configured to accept websocket connections from outside.
   You must ensure that the configuration is secure.

* ARI connection **must** be secured. Open file :file:`/etc/asterisk/ari.conf` and check if the default password
  ``Nasheow8Eag`` was changed. If not, change it:

    * Generate a password (e.g. with ``pwgen -s 16``)
    * replace::

       password = Nasheow8Eag

      by::

       password = <YOUR_GENERATED_PASSWORD>

* Open file :file:`/etc/asterisk/http.conf` and check if ``bindaddr`` option is set to ``0.0.0.0``
  (to accept outside websocket connections).

* You should also secure (e.g. via an external firewall) access to the asterisk HTTP server (which listens on port 5039).


WebSocket connection limit
==========================

By default, asterisk HTTP server has a limit of 100 websocket connections. You can change this limit in the :file:`/etc/asterisk/http.conf` file::

   sessionlimit=200

Restart XiVO PBX services to apply the new settings::

   xivo-service restart

