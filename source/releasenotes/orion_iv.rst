********************************
XiVO Orion Intermediate Versions
********************************

2025.03.00 (IV Orion): February 2025
------------------------------------

Consult the `2025.03.00 (IV Orion) Roadmap <https://projects.xivo.solutions/versions/598>`_.

Components updated:


Debian:
asterisk, xivo-config, xivocc-installer, xivo

Docker:
config-mgt, edge-nginx, xivo-edge, xivo-full-stats, xivo-jicofo-jitsi, xivo-jigasi-jitsi, xivo-jvb-jitsi, xivo-meetingrooms, xivo-prosody-jitsi, xivo-web-jitsi, xucmgt, xucserver

Others:
xivo-desktop-assistant

**Asterisk**

* `#8019 <https://projects.xivo.solutions/issues/8019>`_ - 🛠️❗️ - Upgrade to asterisk 22

  .. important:: **Behavior change** With asterisk 22 it is no longer possible to activate `chan_sip`.
    Also the application `Monitor` was removed: existing dialplan must use the `MixMonitor` application instead.


**Desktop Assistant**

* `#7798 <https://projects.xivo.solutions/issues/7798>`_ - 💣️ - Answer Call from Headset 

**Edge**

* `#8177 <https://projects.xivo.solutions/issues/8177>`_ - Be able to make TURN server listen on port 443

**Meeting Room**

* `#8152 <https://projects.xivo.solutions/issues/8152>`_ - Upgrade Jitsi component

  .. important:: **Behavior change** Jitsi components upgraded to version 9909

    This new version includes, among other things, the following updates and behavior changes

      * Video calls are now supported on Firefox.
      * The ENABLE_BACKGROUND_SELECTION config key is removed. Background selection is now enabled by default
      * We updated our Jitsi integration policy: previously, only basic functionalities were enabled. Now, all features are enabled except those that are not working (e.g., sharing links).
      * Re-enabled self-view in conference rooms. Users can revert to the previous behavior in Jitsi options.
      * Introduced new Jitsi features: breakout rooms, local recording in Chrome, and more.
      * Removed unnecessary optimizations from the Jitsi configuration, as they are no longer needed in the new Jitsi version.

**Recording**

* `#8174 <https://projects.xivo.solutions/issues/8174>`_ - Some recordings are not visible through recording server

**Reporting**

* `#8170 <https://projects.xivo.solutions/issues/8170>`_ - Stat - Call thread id is broken when user_id is void

**System**

* `#8163 <https://projects.xivo.solutions/issues/8163>`_ - API Group - Group members are not managed by group CRUD APIs

**UC App**

* `#8097 <https://projects.xivo.solutions/issues/8097>`_ - As a UC user I want to be able to see the groups to which I belong
* `#8116 <https://projects.xivo.solutions/issues/8116>`_ - As a UC user I want to be able to activate/deactivate calls from a group
* `#8187 <https://projects.xivo.solutions/issues/8187>`_ - I want to be able to make a consultation call while I'm in an audio conference

**WebRTC**

* `#8185 <https://projects.xivo.solutions/issues/8185>`_ - Set Ice Gathering to 500ms and not 2s by default

  .. important:: **Behavior change** Reduce the ice gathering to 500ms by default


**XiVO PBX**

* `#8088 <https://projects.xivo.solutions/issues/8088>`_ - Bridge - Must wait at least a relay candidate

**XiVOCC Infra**

* `#8069 <https://projects.xivo.solutions/issues/8069>`_ - xivo-populator


2025.02.00 (IV Orion): January 2025
-----------------------------------

Consult the `2025.02.00 (IV Orion) Roadmap <https://projects.xivo.solutions/versions/597>`_.

Components updated:

Docker:
config-mgt, xivo-web-interface, xucmgt, xucserver

Debian:
xivo-config, xivocc-recording, xivo, xivocc-installer

**Recording**

* `#8046 <https://projects.xivo.solutions/issues/8046>`_ - Recording - Move our recording feature to use MixMonitor

  .. important:: **Behavior change** The recording file starts at the first recorded segment of a call. If the recording is paused during the call, then a blank is present in the recording file for the duration of the pause (previously, those blank would be cut leading to such pause to be harder to track).
    
    Custom Dialplan must now use MixMonitor.


**UC App**

* `#8100 <https://projects.xivo.solutions/issues/8100>`_ - As a user, I want the configuration menu sections of the UC Assistant to be clearer so that I can better understand their functionality.
* `#8112 <https://projects.xivo.solutions/issues/8112>`_ - Display the call group the user belongs to with live update

**XiVO PBX**

* `#8143 <https://projects.xivo.solutions/issues/8143>`_ - xivo-config update should reload templated dialplan
* `#8146 <https://projects.xivo.solutions/issues/8146>`_ - web interface database access logging



2024.11.00 (IV Orion): January 2025
-----------------------------------

Consult the `2024.11.00 (IV Orion) Roadmap <https://projects.xivo.solutions/versions/588>`_.

Components updated:


Debian:
asterisk, xivo-ci, xivo-config, xivocc-installer, xivo
Docker:
config-mgt, xivo-agid, xivo-web-interface, xucmgt, xucserver

Others:
ldap-test, sipml5-xivo-mirror

**Asterisk**

* `#8086 <https://projects.xivo.solutions/issues/8086>`_ - [S] Study impact of recent Asterisk vulnerabilities - CVE-2024-42491 / CVE-2024-35190 / CVE-2024-42365
* `#8108 <https://projects.xivo.solutions/issues/8108>`_ - Update asterisk to 20.11.0
* `#8109 <https://projects.xivo.solutions/issues/8109>`_ - Update asterisk to 18.26.0

**CC Agent**

* `#8133 <https://projects.xivo.solutions/issues/8133>`_ - Update phone number after logout impossible on CCAgent and Switchboard

  .. important:: **Behavior change** - Agents can now change the phone number they are connected to using a dedicated button in the CC Agent application. See :ref:`switch-phone-number`
    - Agents no longer have to re-enter their login and password if they attempt to connect with an incorrect number or are already connected to a different number.


**CCAgent**

* `#8119 <https://projects.xivo.solutions/issues/8119>`_ - Call qualification does not open sometimes

**CTI**

* `#8130 <https://projects.xivo.solutions/issues/8130>`_ - Aastra phone auto answer in the speaker instead of the headset

**UC App**

* `#7367 <https://projects.xivo.solutions/issues/7367>`_ - Only one contact card per person owning the same E-mail address
* `#8066 <https://projects.xivo.solutions/issues/8066>`_ - Cypress helper
* `#8076 <https://projects.xivo.solutions/issues/8076>`_ - As a UC App user I want to be able to visualize my audio device
* `#8111 <https://projects.xivo.solutions/issues/8111>`_ - Display the call group the user belongs to (without live update)
* `#8137 <https://projects.xivo.solutions/issues/8137>`_ - We should not send multiples history requests if a user log in / log out without restart the page
* `#8142 <https://projects.xivo.solutions/issues/8142>`_ - Contact merge should not happen if email is void

**XUC Server**

* `#7765 <https://projects.xivo.solutions/issues/7765>`_ - As a UC Assistant MDS user, when I'm invited in an audio conference, I sometimes don't see who I am in participant list

**XiVO PBX**

* `#8071 <https://projects.xivo.solutions/issues/8071>`_ - Increased missing call dont work when logged out
* `#8128 <https://projects.xivo.solutions/issues/8128>`_ - Can't save a field (userfield or description) containing a simple quote
