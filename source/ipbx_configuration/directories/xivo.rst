.. _xivo-directory:

**************
XiVO Directory
**************

This Directory Type is used to query the users of a XiVO.

| On a fresh install, there is a default XiVO Directory defined.
| Its Directory Name is **xivo** by default.
| Its Directory Source Name is **internal** by default.

| The URI field for this type of directory should be the base URL of a `xivo-confd` server.
| This directory type matches the :ref:`dird-backend-xivo` backend in `xivo-dird`.


Available Fields
================

To map/search on in :ref:`directory-definition`

* id
* agent_id
* line_id
* firstname
* lastname
* email
* exten
* context
* mobile_phone_number
* userfield
* description
* voicemail_number


Example
=======

Adding Directory
----------------

   :menuselection:`Configuration --> Management --> Directories`

Below the default **xivo** Directory

.. figure:: images/xivo_add_directory_xivo.png


Adding Directory Definition
---------------------------

   :menuselection:`Services --> CTI Server --> Directories --> Definitions`

Below the Definition for the xivo Directory, with **internal** as Directory Source Name 

.. figure:: images/xivo_configure_directory_xivo_maia.png

