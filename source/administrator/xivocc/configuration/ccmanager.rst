.. _ccmanager_configuration:

**********
CC Manager
**********

See :ref:`CC Manager features <ccmanager>`.

.. _ccmanager-security:

Access authorizations in CCManager
==================================

.. note:: Behavior was changed in 2017.LTS1 (see 2017.LTS1 release notes in :ref:`xivosolutions_release`)

By default, CCManager access is authorized only for users with *Administrateur* or *Superviseur* rigths (as defined in the
Configuration Management server).
If required, you can authorize all users to connect to the CCManager interface by setting the ``ENFORCE_MANAGER_SECURITY`` environment variable to ``false`` in the
:file:`/etc/docker/compose/custom.env` file::

   ...
   ENFORCE_MANAGER_SECURITY=false

Then you need to recreate the xucmgt container with ``xivocc-dcomp up -d``.
Then each user will be able to log in the CCManager. Otherwise, each user that wants to connect to the CCManager will need to have a *Administrateur* or *Superviseur* profile in the Configuration Management server.

.. _ccmanager-warn-when-spied:

Warn agent when spied
=====================

By default when a supervisor spies on an agent - see :ref:`ccmanager_actions_agent` - the agent is only warned via its :ref:`CC Agent Call control <agent_call_control>` listen icon.
You can change the behavior to warn the agent when spied swith a small beep and/or a function key lit on its deskphone.

To do this, set the ``ENABLE_CHANSPY_BEEP`` environment variable to ``true`` in the :file:`/etc/docker/compose/custom.env` file::

   ENABLE_CHANSPY_BEEP=true

See also :ref:`listen_funckeys`.

.. _ccmanager-configure-statistics:

Configure queue statistics displayed
====================================

You can change the statistics displayed in the set of squares. Note that any configuration will be applied to all queues, those statistics cannot be configured per queue.

To configure it, those keys are to be set in the custom env of your xivocc (you are not forced to set them all) :

- CCM_QINDICATOR_TL for the top left square
- CCM_QINDICATOR_TR for the top right square
- CCM_QINDICATOR_BL for the bottom left square
- CCM_QINDICATOR_BR for the bottom right square

The available statistics are documented in the API and SDK part of the documentation - see :ref:`real-time-queue-statistics` and :ref:`additional-real-time-queue-statistics`

Example of custom.env configuration :

::

   ...
   CCM_QINDICATOR_TL="TotalNumberCallsAnswered"
   CCM_QINDICATOR_TR="TotalNumberCallsAbandonned"
   CCM_QINDICATOR_BL="AvailableAgents"
   CCM_QINDICATOR_BR="WaitingCalls"
   ...

Recording switch for queues
===========================

By default, the button for enabling/disabling queue recording from the CCManager interface is enabled.

If necessary, will hide the switch button to enable / disable recording for all queues by setting the ``SHOW_RECORDING_SWITCH`` environment variable in :file:`/etc/docker/compose/custom.env` file::


   SHOW_RECORDING_SWITCH=false


Then you need to recreate the xucmgt container with ``xivocc-dcomp up -d``.
