*****************
Post Installation
*****************

Here are a few configuration options that are commonly changed once the installation is completed.
Please note that these changes are optional.

Docker Compose Files
====================
XiVO have some services running under docker container.

XiVO docker configuration is stored in ``/etc/docker/xivo`` directory.
Here the list of all the "standard" files which can be present (depends on what you installed):
- ``docker-xivo.yml``
- ``docker-xivo.override.yml`` => present if you have XiVOCC or UC-Addon installed with XiVO
- ``docker-xivo-uc.override.yml`` ==> present if you have UC-Addon installed
- ``docker-xivo-ivr.override.yml`` ==> present if you have IVR Editor installed

We advise you to not touch those files for maintnability reasons (files are replaced in case of upgrade).

If you want to customize docker container definition, you can add files with the following pattern ``[0-9]{2}-.*\.override.yml``.

Example : 00-add-something.override.yml

They will be read after all the "standard" files.



Display called name on internal calls
=====================================

.. note:: Configured by default if you checked the *Apply default onfiguration for France* at the wizard time
          (see :ref:`Wizard configuration step <wizard_configuration>`).

When you call internally another phone of the system you would like your phone to display the name
of the called person (instead of the dialed number only).
To achieve this you must change the following SIP options:

* :menuselection:`Services --> IPBX --> General settings --> SIP Protocol --> Default`:

    * Trust the Remote-Party-ID: yes,
    * Send the Remote-Party-ID: select ``PAI``


Incoming caller number display
==============================

The caller ID number on incoming calls depends on what is sent by your provider.
It can be modified via the :ref:`caller_number_normalization`.


Time and date
=============

* Configure your locale and default time zone device template => :menuselection:`Configuration --> Provisioning --> Template Device`
  by editing the default template
* Configure the timezone in => :menuselection:`Services --> IPBX --> General settings --> Advanced --> Timezone`
* If needed, reconfigure your timezone for the system::

    dpkg-reconfigure tzdata


Codecs
======

.. note:: Configured by default if you checked the *Apply default onfiguration for France* at the wizard time
          (see :ref:`Wizard configuration step <wizard_configuration>`).

You should also select default codecs. It obviously depends on the telco links, the country, the phones, the usage, etc.
Here is a typical example for Europe (the main goal in this example is to select *only* G.711 A-Law instead of both G.711 A-Law and G.711 µ-Law by default):

* SIP : :menuselection:`Services --> IPBX --> General settings --> SIP Protocol --> Signaling`:

    * Customize codec : enabled
    * Codec list::

        G.711 A-Law
        G.722
        G.729A
        H.264

