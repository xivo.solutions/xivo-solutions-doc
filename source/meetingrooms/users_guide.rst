.. _meetingroom_usersguide:

Users' Guide
============

Join Meeting Room from Assistant
--------------------------------

To join a meeting room, you need to look for it from your Assistant applications (UC Assistant, CC Agent, Switchboard).

By typing part of its name it will display:

* your matching personal meeting rooms
* and the matching static meeting rooms.

Then you can click on the call action and on the join action to join the meeting room.

.. figure:: images/mr_search_and_join.gif

Call Internal Users to join Meeting Room
----------------------------------------

You can call a internal user with video support. It will create a temporary meeting room for this call.

To do so:

* find user in assistant
* in the call action, click on the *video call* action
* you automatically enters temporary meeting room
* the other user receives an invitation:

  * when he accepts he will be joined into the call
  * if he rejects or miss the invitation you will be notified


.. figure:: images/mr_call_video.png   

Invite Internal Users to ongoing Meeting Room
---------------------------------------------

When you are in a meeting room you can invite another internal user in it.

To do so:

* join a meetingroom (type the PIN if needed)
* then search in the assistant the user you want to invite
* in the call action, click on the *invite to meetingroom* action
* the other user receives an invitation:

  * when he accepts he will be joined into the conference
  * if he rejects or miss the invitation you will be notified


.. figure:: images/mr_invite_to_mr.gif

.. note:: If a user accepts an invitation to a meeting room while being currently on call, the call will be automatically put on hold. 


Join Meeting Room for External Participants
-------------------------------------------

It's possible to share a link to people that doesn't use the XiVO assistant. Thanks to :ref:`xivo_edge` it is a public link.

To do so, search for a meeting room in your then click on share icon. The link will be copied in your clipboard.

.. figure:: images/mr_share_link.gif

Join Meeting Room from Phone
----------------------------

You can also access a Meeting Room with a phone by dialing ``**MEETING_ROOM_NUMBER`` where ``MEETING_ROOM_NUMBER`` is the Meeting Room number.
You can only access Meeting Room which are configured the XiVO PBX with a number.

Given the XiVO PBX admin user created a Meeting Room:

* :guilabel:`Name`: ProjectManagerConf
* :guilabel:`Display Name`: Project Manager Conf
* :guilabel:`Number`: 5000

Then someone can join the conference via its phone set by dialing: ``**5000``

.. _create_personal_meeting_room:

Create a personal Meeting room
------------------------------

You can create a personal meeting room from the UCAssistant menu, with the name and pincode of your choice.

.. figure:: images/pmr_creation.gif
    :scale: 80%        


Other users can join a personal meeting room in a browser with an invitation link that can be found by the creator
of the room, when clicking on his meetingroom in the search results or favorites (share button).

Search Meeting Room
-------------------

When you search something in your Assistant (UC Assistant/CC Agent/Switchboard)
the result will contain the matching meeting room.

There is a configurable keyword that allows you to list all available rooms. By default it is ``conference`` and ``visio``.

.. note:: For this to work, CTI Display Filter configuration must match the
   :ref:`xivo_meetingroom plugin configuration <dird-backend-xivo_meetingroom>`.
