.. _xds_architecture:

****************
XDS Architecture
****************

The following diagram presents the XDS architecture with:

* one XiVO Main
* one CTI / Reporting Server (XiVO CC)
* and three MDS


.. figure:: xds-architecture.png
   :scale: 100%


As you can see:

* each MDS and the Main are linked together via an internal SIP trunk (the yellow lines) - via the VoIP IP Address of the MDS
* the *xuc* components of the CTI / Reporting Server is connected to each MDS AMI - via the VoIP IP Address of the MDS

.. _xds_archi_db_replic:

Database Replication
====================

Database replication is employed by the mds to receive replicated data from XiVO Main.

- The mds use PostgreSQL replication to synchronize data with XiVO Main.
- To control the replication process, the `max_slot_wal_keep_size` parameter is set to 1G. This means that if a mds becomes unavailable and falls too far behind, it will not be able to continue replication.

For more detailed information on PostgreSQL replication configurations, please refer to the `PostgreSQL documentation <https://www.postgresql.org/docs/current/runtime-config-replication.html>`_.