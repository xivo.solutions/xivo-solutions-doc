
Auth Delegation
===============

Authentication delegation allows the user to be authenticated through an external source, but it does not eliminate the login step for the user.
When using authentication delegation, the user's credentials are verified via an external system, ensuring a secure login process.
However, the user is still required to initiate the login action.

.. contents:: :local:

.. _ldap-xuc-authentication:

Login Timeout Configuration
---------------------------

When configuring external service as authentication provider, you may have trouble to login if the external system is slow. To overcome this issue, you can increase the default timeout (5000 milliseconds default) by setting the `LOGIN_TIMEOUT_MS` parameter in the :file:`custom.env` of your xivocc installation. This value is the delay in milliseconds before aborting a login attempt.

.. code-block:: shell

  LOGIN_TIMEOUT_MS=7000

.. _ldap_authent:

LDAP Authentication
-------------------

Configure LDAP authent for CCmanager, UC Assistant and CC Agent


#. Create a a configuration file named :file:`xuc.conf` to add ldap configuration:

   .. code-block:: bash

      mkdir -p /etc/docker/xuc/
      touch /etc/docker/xuc/xuc.conf
#. Edit file :file:`/etc/docker/xuc/xuc.conf` with the following content:

   .. code-block:: ini

      include "application.conf"

      authentication {
        ldap {
          managerDN = "uid=company,ou=people,dc=company,dc=com"
          managerPassword = "xxxxxxxxxx"
          url = "ldap://ldap.company.com:389"
          searchBase = "ou=people,dc=company,dc=com"
          userSearchFilter = "uid=%s"
        }
      }

   Where

    * ``managerDN`` is the LDAP user name of a user with read rights on the whole LDAP (at least the part which contains the users)
    * ``managerPassword`` is the  password for this user
    * ``url`` is the URL to access the customer LDAP: [ldap|ldaps]://LDAP_HOST:LDAP_PORT (N.B. you MUST specify the port. Usually 389 for ldap and 636 for ldaps). Example:

      * to use LDAP ``ldap://10.32.5.6:389``
      * to use LDAP **s**: ``ldaps://ldap.corp.com:636`` - see also :ref:`ldap_authent_ldaps`
    * ``searchBase`` is the LDAP OU where to look for users
    * ``userSearchFilter`` is the LDAP filter to search for user by a given login (i.e. the filter to search by login)
#. Then customize the :file:`docker-xivocc.yml` file to use this :file:`xuc.conf` configuration by adding a volume and an environment variable to specify the alternate config file location

   .. code-block:: yaml
     :emphasize-lines: 6,9

     xuc:
       image: ...

       environment:
       - ...
       - CONFIG_FILE=/conf/xuc.conf

       volumes:
       - /etc/docker/xuc:/conf
#. Finally recreate the container:

   .. code-block:: bash

      xivocc-dcomp stop xuc
      xivocc-dcomp rm xuc
      xivocc-dcomp up -d xuc

.. _ldap_authent_ldaps:

Note for LDAPs
^^^^^^^^^^^^^^

As said above to connect to LDAP with a TLS/SSL connection you just need to put ``ldaps`` in the url of the ldap authentication configuration.

Then it should work out of the box **if the LDAP server uses a certificate signed by a known Certificate Authority of the xucserver JDK**.

If the certificate used by the customer LDAP server is not signed by a known Certificate Authority then you need to do the following:

.. note:: You're probably in this case if you get the following error in the xucserver log when someone is trying to log in:

   ::

         play.api.UnexpectedException: Unexpected exception[
            AuthenticationDriverException: Unexpected authentication problem in LdapDriver module,
            error: An error occurred while attempting to connect to server ldap.test.avencall.com:636:IOException(
                LDAPException(resultCode=91 (connect error), errorMessage='An error occurred while attempting to establish a connection to server ldap.test.avencall.com/10.32.0.1:636:
                SSLHandshakeException(PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException:
                unable to find valid certification path to requested target), ldapSDKVersion=6.0.0, revision=524c20f3bbcc0d83fb56b9e136a2fd3a7f60437d'))]

#. The customer needs to give you the fullchain certificate file used by the LDAP server (in the following we will refer to it as :file:`CUSTOMER_LDAP_CERT`),
#. Copy :file:`CUSTOMER_LDAP_CERT` file to the xucserver host,
#. Then copy the :file:`CUSTOMER_LDAP_CERT` file inside the xucserver container:

   .. code-block:: bash

     docker cp PATH/TO/CUSTOMER_LDAP_CERT xivocc_xuc_1:/tmp/
#. Generate a Java Keystore file from this certificate:

   .. code-block:: bash

     xivocc-dcomp exec xuc keytool -import -file /tmp/CUSTOMER_LDAP_CERT -alias ldapcert -keystore /tmp/ldapCertTrustStore

   when prompt:

     * chose a password for the Java Keystore you're creating (you will need it afterwards and we will refer to it in the following as ``LDAP_CERT_KSTORE_PWD``)
     * and answer `yes` to the question *Trust this certificate? [no]:*
#. Then on the xucserver host, create a folder to hold this truststore and copy the trusttore generated inside:

   .. code-block:: bash

     mkdir /etc/docker/xuc-custom-truststore
     docker cp xivocc_xuc_1:/tmp/ldapCertTrustStore /etc/docker/xuc-custom-truststore/
#. Then customize the :file:`docker-xivocc.yml` file to use this custom keystore by adding a volume and JDK option to specify the custom keystore location:

   .. warning:: Take care to:

       * replace LDAP_CERT_KSTORE_PWD by the password you chose at step 4
       * keep the correct Xms and Xmx values as configured on your system

   .. code-block:: yaml
     :emphasize-lines: 5,10

     xuc:
       image: ...

       environment:
       - JAVA_OPTS=-Xms512m -Xmx2048m -Djavax.net.ssl.trustStore=/customSslTrustStore/ldapCertTrustStore -Djavax.net.ssl.trustStorePassword=LDAP_CERT_KSTORE_PWD
       - CONFIG_FILE=/conf/xuc.conf

       volumes:
       - /etc/docker/xuc:/conf
       - /etc/docker/xuc-custom-truststore:/customSslTrustStore
#. Finally recreate the container:

   .. code-block:: bash

      xivocc-dcomp stop xuc
      xivocc-dcomp rm xuc
      xivocc-dcomp up -d xuc

.. _cas-sso-configuration:

CAS Authentication
------------------

To enable CAS authentication and sign delegation feature, you need to have an existing CAS infrastructure. You need to be able to create a service for the XiVOCC environment.

.. warning::

   * The CAS authentication server must be accessible from the user and the server hosting the XiVOCC containers.
   * CAS Server users' username must match the XiVO username to allow login on the XiVOCC applications.
   * The CAS server must support at least CAS Protocol version 2.0.
   * The CAS authentication was **only** validated against CAS Server `*``Apereo Central Authentication Service`` in version ``5.1.8``


XiVOCC Configuration
^^^^^^^^^^^^^^^^^^^^

* Edit the docker compose file ``/etc/docker/compose/docker-xivocc.yml`` to add the following configuration in the xuc section (use your CAS server URL instead of ``https://cas-server.example.org/cas`` and set ``CAS_LOGOUT_ENABLE`` to ``true`` if you want to logout from CAS when logging out from the application):

  .. code-block:: dockerfile

                  xucmgt:
                    # ...
                    environment:
                    - CAS_SERVER_URL=https://cas-server.example.org/cas
                    - ...

                    # ...

                  xuc:
                    # ...
                    environment:
                    - CAS_SERVER_URL=https://cas-server.example.org/cas
                    - CAS_LOGOUT_ENABLE=false
                    - ...

                    # ...

* Recreate and start the XiVOCC environment:

  .. code-block:: shell

     xivocc-dcomp up -d

.. _oidc-sso-configuration:

OpenID Connect (OIDC) Authentication
------------------------------------

To enable OIDC authentication and sign delegation feature, you need to have an existing OpenID Connect 1.0 infrastructure. You need to be able to create a realm and a client for the XiVOCC environment.

.. warning::

  * The OIDC authentication server must be accessible from the user and the server hosting the XiVOCC containers.
  * OIDC Server users' username must match the XiVO username to allow login on the XiVOCC applications.
  * We use Client Authentication with Keycloak (for more details see => https://openid.net/specs/openid-connect-core-1_0-15.html#ClientAuthentication)
  * The OIDC authentication was **only** validated against OIDC Server ``Keycloak`` in version ``10.0.2``.


XiVOCC Configuration
^^^^^^^^^^^^^^^^^^^^

Available env variables
"""""""""""""""""""""""

**Here are the needed env variables to have the OIDC working :**

:guilabel:`ENABLE_OIDC`
  * It defines if the OIDC authentication is enabled
  * If unset, the default value is false
  * The syntax expect a simple boolean

:guilabel:`OIDC_SERVER_URL`
  * It's the address where the UC Assistant redirect the user to be authenticated
  * It challenges the ``iss`` field in the JWT token
  * The syntax expect a single element

:guilabel:`OIDC_CLIENT_ID`
  * It's the client id configured in the OIDC server
  * It challenges the ``azp`` field in the JWT token
  * It also challenges the ``aud`` field in the JWT token if ``OIDC_AUDIENCE`` is not configured
  * The syntax accepts a single element

Configuring XiVOCC
""""""""""""""""""

* Edit the docker compose variables file ``/etc/docker/compose/custom.env`` and add these keys :

 .. code-block:: dockerfile

                 ENABLE_OIDC=true
                 OIDC_CLIENT_ID=myClientId
                 OIDC_SERVER_URL=https://oidc-server.example.org/auth/realms/myrealm

* Recreate and start the XiVOCC environment:

 .. code-block:: shell

    xivocc-dcomp up -d


Optional env variables
""""""""""""""""""""""

**Here are the additional variables for an advanced configuration :**

:guilabel:`OIDC_ADDITIONAL_SERVERS_URL` *optional*
  * It's an additional list of accepted token issuers that is allowed to provide token for authentication
  * Useful if you want to login to the Xuc server with a token issued from a different OIDC server than OIDC_SERVER_URL
  * It challenges the ``iss`` field in the JWT token
  * If unset, only ``OIDC_SERVER_URL`` will challenge this token field, else, both will be used together
  * The syntax accepts a single element, or a list of elements separated by comma

:guilabel:`OIDC_ADDITIONAL_CLIENT_IDS` *optional*
  * It's an additional list of accepted client id
  * Useful if you want to login to the Xuc server with a token issued from a different OIDC server than OIDC_SERVER_URL
  * It challenges the ``azp`` field in the JWT token
  * If unset, only ``OIDC_CLIENT_ID`` will challenge this token field, else, both will be used together
  * The syntax accepts a single element, or a list of elements separated by comma

:guilabel:`OIDC_AUDIENCE` *optional*
  * It's the list of accepted audiences
  * It challenges the ``aud`` field in the JWT token
  * If unset, it will be set to the ``OIDC_CLIENT_ID`` env variable value by default
  * The syntax accepts a single element, or a list of elements separated by comma

:guilabel:`OIDC_USERNAME_FIELD` *optional*
  * It's the field customizer for the XiVO username mapping
  * It will decide the name of the field that will be considered as the XiVO username in the JWT token
  * If unset, the XiVO username will be expected in the ``preferred_username`` token field
  * The syntax expect a single element

:guilabel:`OIDC_LOGOUT_ENABLE` *optional*
  * It defines if logging out of the XiVO application will also log the user out of the SSO
  * If unset, the default value is ``false``
  * The syntax expect a simple boolean

.. _disabling-xivo-authentication:

