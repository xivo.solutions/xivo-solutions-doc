.. _telemetry:

*********
Telemetry
*********

In our telemetry system, we utilize two Docker components:

Usage Writer
============

The Usage Writer monitors system activities, capturing details such as connection events and XiVO platform configurations. This data is then stored in a XiVO database for analysis. Here's a breakdown of what is collected:

- **Configuration:**
  - Presence of edge and switchboard
  - Number of agents, MDS (Managed Data Servers), meeting rooms, switchboards, users, and WebRTC users

- **Events:**
  - User logins
  - Software type (web browser, electron, mobile)
  - Line type (phone, WebRTC, UA)

Usage Collector
===============

The Usage Collector retrieves data from the Usage Writer, refines it into specific metrics, and sends it to our central database. It's important to note that all data sent is anonymized to protect user privacy.

System Architecture and Data Flow
=================================

.. figure:: images/usm_flow.png
   :scale: 100%

This image illustrates the data flow in our telemetry system, showcasing how data moves from the Usage Writer to the central database, undergoing refinement for meaningful analysis. This streamlined flow ensures accurate insights for informed decision-making while safeguarding user anonymity.

Data retention
==============

By default, data in the usage (``usm``) database is kept for the last 365 days (1 rolling year).
This is configured in the crontab :file:`/etc/cron.d/xivo-purge-db`.

The data purge is run every day at 2:25 a.m.
Log of the purge can be seen in the syslog::

  grep xivo-purge-usm /var/log/syslog