.. _mobile_application:

*******************************
Mobile Application :badge:`PRO`
*******************************

.. important:: This is an enterprise version feature that is not included in XiVO community and therefore not freely available due to PUSH mecanism to be set in place to wake up mobile phones.
    To enable it, please contact `XiVO team <https://www.xivo.solutions/contact/>`_.

    Nevertheless these considerations, the project stay **Open Source** and is available here: https://gitlab.com/xivo.solutions/xivo-mobile

**Contents:**

.. contents:: :local:

.. _mobile_app_requirements:

Requirements
============

Minimum Jabbah version : Jabbah.14
----------------------------------

The XiVO mobile application should be used with at least the Jabbah version of XiVO. 
The XiVO must contain an EGDE infrastructure allowing teleworkers or travelling workers to be able to access XiVO from outside.
Follow the official documentation on required ports : See https://documentation.xivo.solutions/en/2022.10/edge/architecture.html#id3

.. figure:: images/mobile-app-schemas.png


When the user receives an incoming call, we use Firebase to wake up the mobile application by sending a push notification for Android devices and uses APNS (Apple Push Notification Server) for iOS devices.

.. figure:: images/archi.png
   :scale: 80%


The official firebase documentation describes the required ports and firewall configuration.
See https://firebase.google.com/docs/cloud-messaging/concept-options#messaging-ports-and-your-firewall

Apple documentation describes the required ports and firewall configuration.
https://support.apple.com/en-la/102266


Limitations
-----------

* No Video call
* No agent account 
* No unique account (except switch between PC softphone or mobile)
* No OIDC, CAS or Kerberos authentication, only basic or LDAP

Compatibility
-------------

* Android: 

The tests are performed from Android 12. Even if compability is set Android from 11, previous versions are not supported.
The application lifecycle is following `Android 3 years LTS support <https://endoflife.date/android>`_ 
Minimal phone requirements to find the application on store are :
* Shipped after 2020
* RAM >= 2Gb

* IOS: 

The tests are performed from devices compatible with at least iOS 13.
Previous versions are not supported.

Case of unlisted terminals 
---------------------------

.. Warning:: Manufacturer’s software layer may create unattended behavior on unsupported devices. Please open a ticket to describe device - with OS version and the bug.

It is imperative to make preliminary tests on the mobiles which would not be compatible. Be aware that some manufacturers are not keen with push notifications, and can cause issues for receiving VOIP calls.
You can find more information about it `here <https://dontkillmyapp.com/>`_ 

If you encounter problems, you can send an email to the support directly via the mobile application. It is however important to have configured on your mobile an application to send emails.
The logs of the mobile application are sent in an attached file in this email.

You can also send logs to your email directly for own debugging, 

.. _mobile_app_push_notifications:

Push Notifications
==================

Push notification is a technology used in the XiVO server and the mobile application.

Since the Izar version, you can ring multiple devices at the same time. 
When you choose to ring the mobile application and the UCAssistant, the system will know when the devices are both ready to receive the incoming call.

As the UC assistant is connected directly to the server via the web interface, this will be relatively quick. 

For the mobile application, the operating systems (Android and IOS) are using a "DOZE" mode. 
This mode closes the application when it has not been used for some time, to reduce the consumption of the battery.
To wake up the application, we use push notification that goes through a public push server either from Google or Apple to wake up the XiVO mobile application. 

You might need to wait a certain time after you launched the call for the mobile application to wake up. This time can vary from one to ten seconds, depending on the push servers and the network quality. 
Once the mobile application responds, we start ringing the available devices.

Some use cases
--------------

Ringing device selected: 

* *UC Assistant*: Fast ringing on the UC direct connection to the web browser or desktop
* *Mobile application*: Launching the ringtone on the mobile application when it will be awakened. The mode of operation used is the push notification. There can be a waiting time.
* *UC + Mobile Application*: There is a delay before launching the ringtone on both devices, the time for the mobile application to wake up.

.. figure:: images/push-notification.png
   :scale: 80%   

.. _mobile_app_troubleshooting:

Troubleshooting 
===============

Error Codes 
-----------

Error codes will be displayed in case of a problem on the application while trying to make an outgoing call : 

* Error code 1: 

This error code means the application is not receiving or sending the SIP signal, and that the user is not REGISTER in the asterisk.  


* Error code 2: 

You will get this error code if the application is able to receive or send the SIP signal, but the user is not REGISTER in the asterisk.


No internet access 
------------------

If the user looses access to the Internet, the application switches to a non-usable mode until access is regained (see screenshot below). 
The connection status display is materialized by a greeen circle when there is an internet access, grey otherwise. If a call is done meanwhile, it will be dialed once internet connection is back.

.. figure:: images/no-internet-mobile-app.png
   :scale: 80%


Sending logs
------------
If you encounter an unsual behaviour and you manage to reproduce it on your application, you can send the logs directly via the configuration and click on *Send logs to support*.

Warning: The logs are sent by email, you must therefore have a messaging system configured on your phone. You can ovveride the email to send it to yourself

.. figure:: images/sending-logs-mobile-app.png
 :scale: 80%

.. figure:: images/sending-logs-mobile-app2.png
 :scale: 80%

**Understanding logs**:

All business code is part of module, some modules have been pinpointed to focus on some app behaviour such as:

* *[Permission]* Related to permission granted by the user to the app
* *[Login]* Login process and token renewal
* *[Notification]* Local Notification for missed call and also remote push management registration/wake up
* *[System]* Low level foreground process to keep the app awake when receiving VOIP calls
* *[WebSockets]* Websockets lifecyle for both Xuc (CTI) and Asterisk (SIP)
* *[Webrtc]*  actions / events related to the protocol
* *[Audio]*  Ringing tones and audio routes


Required permissions 
--------------------

If you didn't give all the authorizations needed to use the mobile app, this error message will be displayed the next time you open the application.
This means the XiVO phone account is not active.

.. figure:: images/required-permissions-mobile-app.png
 :scale: 80%

In this case, you should click the *OK* button and will be redirected to the settings.

If the phone is a "Samsung", the *phone accounts* settings will be opened. You just have to activate the check mark next to XiVO.

.. figure:: images/authorizations-mobile-app.png
 :scale: 80%

If the phone is *not* a Samsung, you will be redirected to the wrong settings. You will need to search in your phone settings where to activate the XiVO phone account.

Xiaomi stop battery optimization 
--------------------------------

.. figure:: images/xiaomi-battery-stop.png
 :scale: 80%

Xiaomi authorizations 
---------------------

In the settings of the Xivo application check if these settings are active: 

Show on lock screen
Show pop-up windows while browsing in the background

.. figure:: images/xiaomi-authorizations.png

If you are in the above context, you may not receive calls when your phone is locked.

Xiaomi phone account
--------------------

When activating the XiVO phone account (step 5), you need to : 

* Click *Ok* on the window  *Les permissions requises*, 
* go to *paramètres d’appel* => *Comptes des appels*,
* then in *comptes téléphoniques*  you need to activate XiVO. 

.. figure:: images/xiaomi-phone-accounts.png
 :scale: 80%

.. figure:: images/xiaomi-phone-accounts2.png
 :scale: 80%

.. _mobile_application_configuration:

Configuration
=============

Type of Ringtone
----------------

You can change the value of `XIVO_MAPP_WAIT_WITH_MUSIC` in the file :file:`/etc/xivo/asterisk/xivo_globals.conf` to customize the ringtone behavior.
If the value is set to `True`, you will hear the default music on hold while attempting to reach to a user with a mobile application. Otherwise, you will hear a regular ringtone.

.. code-block:: python

   XIVO_MAPP_WAIT_WITH_MUSIC = "True"

In case you do want to completely disable this, then you can set the value of `XIVO_PLAY_MSG` to `False` which will make caller hear a regular ringtone.

.. code-block:: python

   XIVO_MAPP_PLAY_MSG = "True"

Waiting While Mobile Application Is Waking Up
---------------------------------------------

Before calling a user with a Mobile App, we wait for the mobile app to wake up (and (re)register (SIP level)).
By default we wait 15s: 3 loops of 5s.

If the Mobile App never wakes up (or never (re)registers) the call will continue depending on the callee ringing device:

* if it was *Mobile application*: it will end up in user's no answer scenario
* if it was *UC Assistant* + *Mobile application*: it will call the *UC Assistant*

The time we wait for the Mobile App to wake up is configurable by configuring the number of loops.Interval between two loops is now configurable too.
To do so, adjust the following two variables in the file :file:`/etc/xivo/asterisk/xivo_globals.conf`:

.. code-block:: python

   XIVO_MAPP_LOOPS_MOBILEAPP = 3
   XIVO_MAPP_LOOPS_WEBAPPANDMOBILEAPP = 3
   XIVO_MAPP_LOOPS_INTERVAL = 5

Note that:

* ``XIVO_MAPP_LOOPS_MOBILEAPP`` configures the number of loops if the callee ringing device is *Mobile application*
* ``XIVO_MAPP_LOOPS_WEBAPPANDMOBILEAPP`` configures the number of loops if the callee ringing device is *UC Assistant* + *Mobile application*
* ``XIVO_MAPP_LOOPS_INTERVAL`` configures the time between two loops

Therefore you might, for example, want to:

* lower the number of loops when ringing device is *UC Assistant* + *Mobile application*: in order to fallback more quickly on the *UC Assistant* if the Mobile App does not wake up
* and increase the number of loops when ringing device is *Mobile application*: in order to give more time to the Mobile App to wake up before going to the callee no answer scenario
* another more dynamic but risky scenario is to disable the message via `XIVO_PLAY_MSG`, then reducing the time between loops via `XIVO_MAPP_LOOPS_INTERVAL`, in this scenario, we advice you not to forget to adapt the loops number to give sufficient time to the MobileApp to wake up.