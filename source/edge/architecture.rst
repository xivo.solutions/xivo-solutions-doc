.. _edge_architecture:

*****************
Edge Architecture
*****************

.. contents:: :local:

Overall Architecture
====================


.. figure:: images/edge_archi_highlevelview.png
   :alt: edge_archi_highlevelview


As you can see on the schema above the Edge Solution is to be put inside one's company
DMZ.

From the external world only the Edge Solution will be seen.
Only HTTPS and TURN flows should be authorized from the external - see :ref:`edge_archi_network` for more details.


The Edge Solution is composed of three components:

* a Web Proxy
* a SIP Proxy
* and a TURN Server

These components can be installed:

* on 3 separate servers - see :ref:`edge_install_3servers`
* or on 1 server - see :ref:`edge_install_1server`


.. _edge_archi_network:

Network Flows
=============


From the outside (WAN to DMZ)
-----------------------------

.. figure:: images/edge_archi_networkflows_external.png
   :alt: network_flows_external_to_dmz

+-----------+--------------------+--------------------+-------------------------------------------------------+
| From      | To                 | Ports to open      | Usage                                                 |
+===========+====================+====================+=======================================================+
| Outside   | Web Proxy          | - TCP/443          | UC Application from External Users                    |
|           |                    |                    |                                                       |
| ``Any``   | ``DMZ_IP1``        |                    |                                                       |
+-----------+--------------------+--------------------+-------------------------------------------------------+
| Outside   | TURN Server        | - UDP/3478         | STUN/TURN via UDP, TCP                                |
|           |                    | - TCP/3478         |                                                       |
| ``Any``   | ``DMZ_IP3``        |                    |                                                       |
+-----------+--------------------+--------------------+-------------------------------------------------------+
| Outside   | TURN Server        | - UDP/30000-39999  | RTP Flow between remote WebRTC client and TURN Server |
|           |                    | - TCP/30000-39999  |                                                       |
| ``Any``   | ``DMZ_IP3``        |                    |                                                       |
+-----------+--------------------+--------------------+-------------------------------------------------------+


Internally (DMZ to LAN)
-----------------------

.. figure:: images/edge_archi_networkflows_internal_dmz2lan.png
   :alt: network_flows_dmz_to_internal

+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| From        | To                         | Ports destination                  | Usage                                                         |
+=============+============================+====================================+===============================================================+
| Web Proxy   | XiVO CC                    | - TCP/443                          | Proxified UC application from Edge towards CC/UC Server       |
|             |                            |                                    |                                                               |
| ``DMZ_IP1`` | ``LAN_IP1``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| TURN Server | Mediaservers (inc. Main)   | - UDP/10000-20000                  | RTP flow between TURN Server and Asterisk(s)                  |
|             |                            |                                    |                                                               |
|             |                            |                                    |                                                               |
| ``DMZ_IP3`` | ``LAN_IP2``, ``LAN_IP3``   |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| SIP Proxy   | Mediaservers (inc. main)   | - UDP/5060                         | SIP flow between Asterisk(s) and SIP Proxy                    |
|             |                            |                                    |                                                               |
|             |                            |                                    |                                                               |
| ``DMZ_IP2`` | ``LAN_IP2``, ``LAN_IP3``   |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+


Supplementary ports for Meetingroom (DMZ to LAN)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you have a :ref:`Meetingroom server <meetingrooms>` in your installation you must also open these ports from the DMZ towards the LAN:

+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| From        | To                         | Ports to open                      | Usage                                                         |
+=============+============================+====================================+===============================================================+
| Web Proxy   | XiVO                       | - TCP/443                          | Proxified Meetingroom API from Edge towards XiVO Server       |
|             |                            |                                    |                                                               |
| ``DMZ_IP1`` | ``LAN_IP2``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| TURN Server | Meetingroom                | - UDP/10000                        | RTP flow between TURN Server and Meetingroom (when using TURN)|
|             |                            |                                    |                                                               |
|             |                            |                                    |                                                               |
| ``DMZ_IP3`` | ``LAN_IP4``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+


Internally (LAN to DMZ)
-----------------------

.. figure:: images/edge_archi_networkflows_internal_lan2dmz.png
   :alt: network_flows_internal_to_dmz


Here are the flow to open between the LAN towards the Edge servers (**LAN to DMZ**):

+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| From        | To                         | Ports to open                      | Usage                                                         |
+=============+============================+====================================+===============================================================+
| UC Clients  | Web Proxy                  | - TCP/443                          | UC Application for Internal Users                             |
|             |                            |                                    |                                                               |
| ``Any LAN`` | ``DMZ_IP1``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| UC Clients  | TURN Server                | - UDP/3478                         | STUN/TURN requests between UC Clients and TURN Server         |
|             |                            | - TCP/3478                         |                                                               |
| ``Any LAN`` | ``DMZ_IP3``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| Mediaservers| SIP Proxy                  | - UDP/5060                         | SIP flow between Asterisk(s) and SIP Proxy                    |
| (inc. Main) |                            |                                    |                                                               |
|             |                            |                                    |                                                               |
| ``LAN_IP2``,|                            |                                    |                                                               |
| ``LAN_IP3`` | ``DMZ_IP2``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| Mediaservers| TURN Server                | - UDP/3478                         | STUN/TURN requests between Mediaservers and TURN Server       |
| (inc. Main) |                            | - TCP/3478                         |                                                               |
|             |                            |                                    |                                                               |
| ``LAN_IP2``,|                            |                                    |                                                               |
| ``LAN_IP3`` | ``DMZ_IP3``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| Mediaservers| TURN Server                | - UDP/30000-39999                  | RTP flow between Asterisk(s) and TURN Server                  |
| (inc. Main) |                            | - TCP/30000-39999                  |                                                               |
|             |                            |                                    |                                                               |
| ``LAN_IP2``,|                            |                                    |                                                               |
| ``LAN_IP3`` | ``DMZ_IP3``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+


Supplementary ports for Meetingroom (LAN to DMZ)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you have a :ref:`Meetingroom server <meetingrooms>` in your installation you must also open these ports from the LAN towards the DMZ:

+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| From        | To                         | Ports to open                      | Usage                                                         |
+=============+============================+====================================+===============================================================+
| Meetingroom | TURN Server                | - UDP/3478                         | STUN/TURN requests between Meetingroom and TURN Server        |
|             |                            | - TCP/3478                         |                                                               |
|             |                            |                                    |                                                               |
| ``LAN_IP4`` | ``DMZ_IP3``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| Meetingroom | TURN Server                | - UDP/30000-39999                  | RTP flow between Meetingroom and TURN Server                  |
|             |                            | - TCP/30000-39999                  |                                                               |
|             |                            |                                    |                                                               |
| ``LAN_IP4`` | ``DMZ_IP3``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+


Inside DMZ
----------

+--------------+-------------+---------------+-------------------+
| From/To      | To/From     | Ports to open | Usage             |
+==============+=============+===============+===================+
| Web Proxy    | SIP Proxy   | - TCP/443     | Websocket for SIP |
|              |             |               |                   |
| ``DMZ_IP1``  | ``DMZ_IP2`` |               |                   |
+--------------+-------------+---------------+-------------------+


This is probably unneeded (traffic between servers inside the DMZ should be in most of the cases
unfiltered), but if it would be the case this flow must be authorized.

