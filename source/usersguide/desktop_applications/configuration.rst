.. _desktop-application-configuration:

*************
Configuration
*************


On first launch the application will display the settings page and ask for the application server.
Basically it should be the IP address of your server
If you don't know it, you need to ask your system administrator or refer to :ref:`dapp_protocol_app_server` paragraph.


Navigation
==========

.. figure:: navigation.png
   :scale: 100%

The top menu (accessible through tray icon or right click on the logo) allows you to navigate either to the application or to the settings page. If you did not enter any setting, the application will redirect you to the settings page.

Zoom
====

.. figure:: zoom.png
   :scale: 100%

A zoom feature is available on the desktop assistant. To zoom in and zoom out click on zoom icons at the top right of the application.
To reset the zoom, open the top menu (accessible through tray icon or right click on the logo) and click on "Zoom reset".

About
=====

By clicking the **?** menu you will open a popup that show you technical information about the application that can be used to report bugs.


Settings
========

Settings page is either accessible from top menu or by clicking directly on the wrench icon in topmost right bar.

.. figure:: settings.png
   :scale: 100%

.. _dapp_user_interface:

User Interface
--------------

Choose if you want to use the Desktop Assistant to access the UC Assistant application or the CC Agent application.

Application Options
-------------------

* **Launch at startup** if enabled, the app starts automatically when you log in to your machine.
* **Close in tray** if enabled, the app stays running in the notification area after app window is closed.


.. _dapp_global_key:

Global keyboard shortcut and Select2Call
----------------------------------------

This field allow you to define one shortcut in application to handle all basic actions that can be done for managing calls. With one combination keypress you should be able to:

* **Call** the phone number contained in your clipboard (a.k.a **Select2Call**)
* **Answer** a call if phone is ringing
* **Hangup** if you are already on call

.. note::
  | To be able to **call** someone, you **must** beforehand have copied in your clipboard a phone number from any source (web page, e-mail, text document...)

   * **Linux**: select phone number then trigger `shortcut`
   * **Windows**:  select phone number, type ``Ctrl+C`` then trigger `shortcut`

  Default **Select2Call** shortcut is ``Ctrl+Space`` for **Windows** and **Linux**, you can either change it or disable it by leaving the field blank.

.. warning:: You must be logged in for using global shortcut and automatic dialing to work.


.. _dapp_protocol_app_server:

Protocol and Application Server
-------------------------------

In these two fields you need to specify the protocol and address to reach the application.
The table below list the possible value:

+--------------------------------+------------------------------------------------------------+
|                                |                      *Connection URL*                      |
+--------------------------------+--------------------------+---------------------------------+
| *Protocol*                     | **Secure (recommended)** | Non Secure (should not be used) |
+--------------------------------+--------------------------+---------------------------------+
| *Application server*           | ``XiVOCC_IP``            | ``XiVOCC_IP:8070``              |
+--------------------------------+--------------------------+---------------------------------+

Note that ``XiVOCC_IP`` or ``XiVOCC_IP:8070`` can be replaced by a FQDN if your administrator has set one and must **not** be prefixed by a protocol (e.g. https)

.. _dapp_call_url:

Handling callto: and tel: URLs
------------------------------

The *Desktop Application* can handle telephone number links that appear in web pages. The *Desktop Application* will
automatically dial the number when you click on a link.

It is supported on both Windows and Linux Debian based distributions (with a desktop environment compatible with Freedesktop_).

.. _Freedesktop: https://www.freedesktop.org/wiki/


.. _dapp_inifile:

Config file
-----------

You can specify your Desktop Assistant parameters by providing a :file:`xivoconfig.ini` file or by editing the one that gets created automatically
on first launch.

.. note::
  | For Windows, this file must be located in user's **%APPDATA%/xivo-desktop-assistant/application/config**.
  |
  | For Linux, this file must be located in user's **$HOME/.config/xivo-desktop-assistant/application/config**.

Below is an example of `xivoconfig.ini` file keys in use:

.. code-block:: ini

 APP_PROTOCOL=HTTPS

 APP_DOMAIN=xivo.mycorp.com

 APP_INTERFACE=UCASSISTANT

 APP_STARTUP=false

 APP_CLOSE=true

 [ALT_SERVERS]
 My favorite server = server-favorite.com
 Another server = some-other.server.com
 My localhost server = 127.0.0.1:8070

APP_PROTOCOL
   | Define the use of HTTPS or HTTP connection.
   | Values: ``HTTP`` or ``HTTPS`` (default to ``HTTPS``)

APP_DOMAIN
   | The server used by the Desktop Assistant.
   | Either an URL (e.g. xivo.mycorp.com) or an IP:PORT (e.g. 192.168.0.1:8070)

APP_INTERFACE
   | Which application to open between UC Assistant or CC Agent.
   | Values: ``UCASSISTANT`` or ``CCAGENT``

APP_STARTUP (optional)
   | Enable open at startup of OS.
   | Values: ``true`` or ``false``

APP_CLOSE (optional)
   | Enable minimization in the taskbar when closing the application.
   | Values: ``true`` or ``false``

[ALT_SERVERS] (optional)
   | Override APP_DOMAIN to enable the use of a preset server list dropdown instead of a text input in the settings menu.
   | One pair of name-address per line.
   | Values: ``SERVER NAME = SERVER ADDRESS``

.. _dapp_update:

Update
======

On Windows, the application will check at startup for a new version of the application and offer to upgrade if one is available.

.. note:: The update will always use the protocol selected in the application settings.

.. important:: To have automatic update working on secured protocol, you need a valid SSL certificate (see :ref:`nginx-trusted-certificate`).

.. figure:: update.png
   :scale: 100%

On Debian, the update relies on the package manager behavior. However you can check for any update by issuing the following commands:

.. code-block:: bash

                sudo apt-get update
                apt-cache policy xivo-desktop-assistant


Startup options
===============

The Desktop Application can be started with following options:

* ``--ignore-certificate-errors`` to disable certificate verification, this option is meant **only** for test purposes. You can use it with self-signed certificates.
* ``-d`` to enable debug menu items
* ``-t`` with an authentication token as parameter (see :ref:`dapp_autolog_wtoken`)

.. note:: On **Windows**  both options must be set to the shortcut ``xivo-desktop-assistant.exe`` pointing to application located in ``C:\Users\<USER>\AppData\Local\xivo\xivo-desktop-assistant.exe`` so that **Target** of shortcut looks like for example to:
  ``C:\Users\IEUser\AppData\Local\xivo\xivo-desktop-assistant.exe --ignore-certificate-errors -d``

.. _dapp_autolog_wtoken:



Custom user data directory
--------------------------

It is possible to set an environment variable named ``CUSTOM_USER_DATA`` where will be stored application configuration. This is usefull if you want for example two shortcuts with two distincts configuration (one for `UC Assistant` and one for `CC Agent`) on the same desktop application installed.

On **Windows** here an example of two shortcuts that set the environment variable (**uc** or **cc**) and launch the application. Following lines must be updated with your **Windows** user name and correct `XiVO` version and must be set in ``Target`` field of the shortcut).

`UC assistant`:

.. code-block:: none

  C:\Windows\System32\cmd.exe /V /C "SET CUSTOM_USER_DATA=C:/Users/IEUser/AppData/Local/xivo-desktop-assistant/uc&& START /D ^"C:\Users\IEUser\AppData\Local\xivo-desktop-assistant\app-2018.7.0^" xivo-desktop-assistant.exe"

`CC ccagent`:

.. code-block:: none

  C:\Windows\System32\cmd.exe /V /C "SET CUSTOM_USER_DATA=C:/Users/IEUser/AppData/Local/xivo-desktop-assistant/cc&& START /D ^"C:\Users\IEUser\AppData\Local\xivo-desktop-assistant\app-2018.7.0^" xivo-desktop-assistant.exe"

.. note:: You can set `Run` mode to `Minimized` in shortcut `General` tab to avoid **cmd.exe** blinking at startup.

Chrome integration
==================

If you don't want to have a Chrome popup like this one when you click each time on a `tel:` link on **Windows** to make a call through your desktop application

.. figure:: chrome_tel.png
   :scale: 100%

You can edit your registry base to modify this Chrome key to avoid this display each time you want to call someone from a webpage

.. code-block:: shell

                [HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Chrome]
                "ClickToCallEnabled"=dword:00000000

Known limitations
=================

* Click on links using protocol `tel:` on **Windows** may not work if any version of `Skype` / `Lync` is installed on the PC.
* Autologin by token does not work with CCAgent because a phone number is required

.. _desktop_troubleshoot:

Troubleshoot Application
========================

Logging
-------

Logs of application are available in order to debug a crash at startup or during usage of the application. 
They do not contain Javascript console logs, but only the interactions with operating system, like global 
shortcut key or callto and tel protocol.

.. note::
  | On Windows, logs are located in user's **%APPDATA%/xivo-desktop-assistant/application/logs**.
  |
  | On Linux, logs are located in user's **$HOME/.config/xivo-desktop-assistant/application/logs**.


Desktop Application Developper Tools
------------------------------------

If needed, you can use the developper console to diagnose a problem "live".
To access the console, right click on the xivo cloud on the top left corner of your application

.. figure:: application_cloud.png
  :scale: 100%

Then, in the 'debug' menu, you can either open the devtool, or the webview's devtool

The normal devtool is the one for the electron application. In this one, you will moslty find errors about shortcut keys, 
callto and tel protocol, network, and everything system-related.

The webview devtool is the one for what's opened inside the desktop application, so the ccagent or the ucassistant. this
devtool is the same one that you get when you open the devtool from the web application in Chrome. You will mostly find network errors or
the errors that are specific to the ucassistant and ccagent features.

The main reason to open devtools is to display the console, but you have another functionality used by the desktop application that you
might need to debug : the localstorage. It's located in the application tab of the electron devtool. To check the variables set there, you need
to click localstorage on the left, and click file://.

.. figure:: application_localstorage.png
  :scale: 70%

The variables that are set in there are the one coming from the xivoconfig.ini (except for the shortcut) and could be the cause of a wrong configuration.


Desktop Application Useful shortcuts
------------------------------------

Ctrl + Shift + D toggle the docking of the devtool.

Ctrl + Shift + R refresh the page and clear the cache at the same time.

Ctrl + L clear the console.

Ctrl + Q kill the application.

Certificate
-----------

If you don't succeed to reach login page of desired application (i.e. just give you the possibility to retry or to change parameters) and if you observe some errors about certificate in debug mode, you should

* Check that you installed correctly the certificate under `/etc/docker/nginx/ssl` (:ref:`webrtc-ssl-cert`).
* Take care if your move a `*.cer` to `*.crt`. You must concatenate a key file to your `*.cer` (``cat certifcate.cer certificate.key > certificate.crt``). Just rename it will not work
* Check that XUC_HOST in `/etc/docker/compose/custom.env` is also configured with the same FQDN as in the certificate, not the IP address.
