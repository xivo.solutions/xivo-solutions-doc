
.. _meetingroom_adminsguide:

Admin's Guide
=============

.. _meetingroom_admin_create:

Create a static Meeting Room
----------------------------

In order to create a static meeting room in XiVO (meaning available for all the users of the XIVO)
go to :menuselection:`Services --> IPBX --> IPBX Settings --> Meeting rooms`

.. figure:: images/smr_creation.png
   :scale: 100%

Create an incoming calls for Meeting Rooms
------------------------------------------

In order to use built-in IVR for Meeting Rooms in XiVO PBX, create an incoming call that will be available for remote users to join MR externally
go to :menuselection:`Services --> IPBX --> IPBX Settings --> Call Management --> Incoming Calls`

* :guilabel:`DID`: your incoming call extension
* :guilabel:`Context`: your incoming calls context (default is from-extern)
* :guilabel:`Destination`: Customized
* :guilabel:`Command`: Goto(meetingrooms-audio-ivr,s,1)
