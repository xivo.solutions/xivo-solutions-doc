********************
System Configuration
********************

.. contents:: :local:

.. toctree::
   :maxdepth: 2

   SSO <sso/sso>
   Auth Delegation <auth_delegation/auth_delegation>

.. _configuration_override:

Configuration Override
======================

Our dockerized scala applications working with an :file:`application.conf` file can have some of their variables overriden by providing the adequate configuration.
You have to :

#. define a custom :file:`.conf` file with the extra configuration you want
#. define a :file:`.override.yml` in :file:`/etc/docker/compose` file to mount the conf file within the docker container, having specifically:

  #. the **CONFIG_FILE** environment variable with the location of the new conf file within the container
  #. a volume mounting your local .conf file to the appropriate location in the container

XucMgt Configuration Override
-----------------------------

#. Define or complete an .override.yml file like :file:`/etc/docker/compose/50-xucmgt.override.yml`:

   .. code-block:: yaml
      :emphasize-lines: 4,6

      services:
        xucmgt:
          environment:
          - CONFIG_FILE=/opt/docker/conf/custom/xucmgt.conf
          volumes:
          - /etc/docker/xucmgt:/opt/docker/conf/custom

#. Create the mounted override conf file, :file:`/etc/docker/xucmgt/xucmgt.conf` in the example, and add your custom configuration:

   .. code-block:: bash
      :emphasize-lines: 3

      include "application.conf"

      # Put here the custom configuration


Xuc Server Configuration Override
---------------------------------

#. Define or complete an .override.yml file like :file:`/etc/docker/compose/66-xuc.override.yml`:

   .. code-block:: yaml
      :emphasize-lines: 4,6

      services:
        xuc:
          environment:
          - CONFIG_FILE=/opt/docker/conf/custom/xuc.conf
          volumes:
          - /etc/docker/xuc:/opt/docker/conf/custom

#. Create the mounted override conf file, :file:`/etc/docker/xuc/xuc.conf` in the example, and add your custom configuration:

   .. code-block:: bash
      :emphasize-lines: 3

      include "application.conf"

      # Put here the custom configuration


Disabling XiVO based authentication
===================================

If you configured another kind of authentication, like :ref:`ldap-xuc-authentication` or :ref:`kerberos-configuration` for example, you can enforce this authentication mechanism by preventing fall back on the XiVO authentication mechanism. In order to achieve that, you need to perform the following configuration.

#. Override the configuration as explained in :ref:`configuration_override` for xuc
#. In the custom :file:`application.conf` file created, add this:

   .. code-block:: ini
      :emphasize-lines: 3-5

      include "application.conf"

      authentication {
         xivo = ""
      }

Recreate the containers according to the new configuration : ``xivocc-dcomp up -d``


.. _nginx_customization:

Nginx Customization
===================

.. _nginx_customization_extend_configuration:

Extend the Configuration
------------------------

(*Since Naos.03*)

It is possible to extend the Nginx configuration.
Our Nginx is split under 3 server_:

* one on port 80,
* one on port 443 with SSL,
* one on port 8443 with SSL

It is possible to extend each of these server_ section. It is mainly useful to add some location_.


#. Create a directory on your UC server:

   .. code-block:: bash

     mkdir -p /etc/docker/nginx/extra-conf.d/{http,https,8443}/
#. Create an override file :file:`/etc/docker/compose/15-nginx.override.yml` for Nginx with the following content:

   .. code-block:: yaml
      :emphasize-lines: 4

      services:
        nginx:
          volumes:
          - /etc/docker/nginx/extra-conf.d/:/etc/nginx/templates/extra-conf.d/
#. Now add you can extend one of the server configuration section. For example:

   * create a file :file:`/etc/docker/nginx/extra-conf.d/https/xivo-api.conf.template`
   * with the folowing content:

     .. code-block:: ini
  
        location /xivo-api {
          proxy_pass http://${NGINX_XIVO_HOST}/api;
        }
   * this will:
     
     * add a location ``/xivo-api`` in the https section (so it will be available when accessing the UC Nginx on port 443)
     * this location is "proxy passed" to the XiVO ``/api`` location on XiVO

.. _server: https://nginx.org/en/docs/http/ngx_http_core_module.html#server
.. _location: https://nginx.org/en/docs/http/ngx_http_core_module.html#location


.. _nginx_path_distribution:

Nginx path distribution
=======================

XiVO CC services can be installed on separate servers, but they can be opened through nginx
as if they were running on the same server:

+--------------+-------------------------------+---------------------------------------------------------------+
| Service      | URL                           | Target URL (should not be used)                               |
+==============+===============================+===============================================================+
| Fingerboard  | \https://XUC_HOST/fingerboard | \http://XUC_HOST/                                             |
+--------------+-------------------------------+---------------------------------------------------------------+
| UC Assistant | \https://XUC_HOST/            | \http://XUC_HOST/                                             |
+--------------+-------------------------------+---------------------------------------------------------------+
| CC Agent     | \https://XUC_HOST/ccagent     | \http://XUC_HOST:8070/ccagent                                 |
+--------------+-------------------------------+---------------------------------------------------------------+
| CC Manager   | \https://XUC_HOST/ccmanager   | \http://XUC_HOST:8070/ccmanager                               |
+--------------+-------------------------------+---------------------------------------------------------------+
| Config Mgt   | \https://XUC_HOST/configmgt   | \http://XIVO_HOST:9100/configmgt                              |
+--------------+-------------------------------+---------------------------------------------------------------+
| Kibana       | \https://XUC_HOST/kibana      | \http://XUC_HOST/kibana                                       |
+--------------+-------------------------------+---------------------------------------------------------------+
| Recording    | \https://XUC_HOST/recording   | \http://RECORDING_SERVER_HOST:RECORDING_SERVER_PORT/recording |
+--------------+-------------------------------+---------------------------------------------------------------+
| SpagoBI      | \https://XUC_HOST/SpagoBI     | \http://REPORTING_HOST:9500/SpagoBI                           |
+--------------+-------------------------------+---------------------------------------------------------------+


ACD Outgoing Calls For Call Blending
====================================

Use the following only if you want to use "Least Recent" call distribution strategy and
that outbound agent calls have to be taken into account by the distribution strategy.

By default, when your agents process incoming and outgoing calls, the call distribution will not take into account agents
which are in outgoing calls in the *least recent* call strategy and at the end of an outgoing call there is no wrapup.
So an agent can be distributed just after an outgoing calls even if another agent is free for a longer time, because the
outgoing call is not taken into account by the distribution strategy.

You will find below how to improve that.

XiVO-CC agent can make outgoing calls through an outgoing queue. This brings the statistics and
supervision visualization for outgoing ACD calls. However, some special configuration steps are
required. The outgoing calls must be dialed from CC Agent application to use this feature.


How Does it Work ?
------------------

#. Pre-condition:

   * create a queue whose name starts with **out**
   * agent uses CC Agent application
#. Then, as soon as an agent (using CC Agent application) dials a number with more digits than ``xuc.outboundLength``, then

   * the call is routed to the agent **via** the outbound queue
   * therefore its queue statistics will be updated


.. note:: The ``xuc.outboundLength`` default value is 6.
   You can customize this value by creating a custom :file:`application.conf` file and changing the
   ``xuc.outboundLength`` value (see :ref:`configuration_override` for xuc).


Configuration Steps
-------------------

* You need to create an outgoing queue with

  * in tab *General*:

    * :guilabel:`Name`: starting with 'out', e.g. outbound,
    * :guilabel:`Number`: some number
    * :guilabel:`Music On-Hold`: None
    * :guilabel:`Preprocess subroutine`: xuc_outcall_acd
  * in tab *Application*:

    * :guilabel:`Ringing Time`: 0
    * :guilabel:`Ring instead of On-Hold music`: activated

* Agent will have to be logged on this queue

How to check correct configuration
----------------------------------

Check if agent is logged in the outbound queue:

.. code-block:: bash

      jyl-rennes*CLI> queue show outbound
      outbound has 0 calls (max unlimited) in 'ringall' strategy (0s holdtime, 0s talktime), W:0, C:0, A:0, SL:0.0% within 0s
      Members:
        Agent/2500 (Local/id-19@agentcallback from SIP/ihvbur) (ringinuse disabled) (dynamic) (Not in use) (skills: agent-19) has taken no calls yet
      No Callers


Check the skills attached to the agent by displaying it's agent group:

.. code-block:: bash

      jyl-rennes*CLI> queue show skills groups agent-19
      Skill group 'agent-19':
        - agent_19       : 100
        - agent_no_2500  : 100
        - genagent       : 100

If the agent dials an outbound call of more than 6 digits (default) you should see the internal queue statistics updated.
The agent's state should be "(in call)" for ongoing call and when the call ends, the number of taken calls should be incremented:

.. code::

    jyl-rennes*CLI> queue show outbound
    outbound has 0 calls (max unlimited) in 'ringall' strategy (0s holdtime, 34s talktime), W:0, C:1, A:0, SL:100.0% within 0s
       Members:
          Agent/2500 (Local/id-19@agentcallback from SIP/ihvbur) (ringinuse disabled) (dynamic) (Not in use) (skills: agent-19) has taken 1 calls (last was 1 secs ago)
       No Callers


Once done, calls requested by an agent through the Cti.js with more than 6 digits are routed via
the outgoing queue. You can change the number of digits using the parameter xuc.outboundLength
in the XuC's configuration.

.. _allow_xuc_login:

Allowing xuc user to login to API or Applications
=================================================

By default, the 'xuc' user can only be used internaly by the xucserver to connect to the xivo. We strongly advise you shouldn't change this behavior but if you need to enable it, you can do it by setting the following variable in the :file:`custom.env` file of your xivocc server.

::

   AUTH_PREVENT_XUC_LOGIN=false

.. _nginx-trusted-certificate:

Install trusted certificate for nginx
=====================================

To install a trusted certificate for the nginx reverse proxy instead of the self signed certificate, follow the following instructions:

* in directory ``/etc/docker/nginx/ssl`` replace content of files

  * :file:`xivoxc.crt`,
  * :file:`xivoxc.csr`,
  * :file:`xivoxc.key` while keeping filenames unchanged
* reload nginx container by command::

   xivocc-dcomp reload nginx

* you should then check that the certificate chain is complete (especially for the XiVO Mobile Assistant) - see :ref:`nginx-complete-cert-chain`.


.. _nginx-complete-cert-chain:

What is a complete certificate chain
------------------------------------

You can check the server certificate chain by using the following web site https://www.ssllabs.com/ssltest/analyze.html which will warn you if there is an error with the certificate (``Chain issues - Incomplete``).

When a client application (browser or mobile application) checks a certificate for a web site, it checks the received certificate is issued by a known certificate authority and matches the web site domain name. But sometimes, the certificate is not issued by a root certificate authority but by an intermediate authority.

Here is an example of a such a certificate chain:

.. code-block:: none

    GeoTrust Global CA
    |--> RapidSSL SHA256 - CA - G3
         |--> *.company.com


The possible problem here is that even if the browser knows the root authority, it is unaware of the intermediate one. The solution is to create a bundle of the complete certificate chain by concatenating the certificates of all parties (root, intermediate & site). Please see http://nginx.org/en/docs/http/configuring_https_servers.html#chains for more information.

Mobile Assistant
================

If using an HTTPS connection for the XiVO Mobile Assistant, you must use a trusted certificate with a complete certification chain, see :ref:`nginx-trusted-certificate`.


Additional Configuration
========================

If you want to expose custom variable by key for your config you can follow these steps:

#. Override the configuration as explained in :ref:`configuration_override` for xucmgt
#. In this custom :file:`application.conf`

   * add your custom variables:

     .. code-block:: bash

        include "application.conf"

        client {
          custom_variable1 = "your_custom_value_1"
          custom_variable2 = "your_custom_value_2"
          # Add more custom variables as needed
        }

   * xucmgt supports nested path for you conf like :

     .. code-block:: bash

        include "application.conf"

        client {
          custom_nested {
            variable1 = "your_custom_value_1"
            variable2 = "your_custom_value_2"
          }
          custom_variable3 = "your_custom_value_3"
          # Add more custom variables as needed
        }

You can acces this value with curl request like this :

.. code-block:: bash

  curl https://XIVOCC_IP/conf/custom_nested/variable1

.. _ami-keep-alive:

AMI Keepalive
=============

The Xuc server monitors the Asterisk AMI connection using periodic pings. These pings ensure regular traffic to keep the session active and help to detect connection instabilities.

The default ping interval is 10 seconds. You can adjust this interval by adding the following variable to the :file:`custom.env` file of your XiVOCC server:
::

   XIVO_AMI_KEEPALIVE=60 # Set the KeepAlive interval to 60 seconds

By default, missed pings are logged in the Xuc logs. You can also configure the AMI connection to restart automatically if 3 consecutive pings are missed. To enable this, add the following variable to the :file:`custom.env` file:
::

   AMI_RECONNECT_ON_MISSED_PINGS=true # Enable automatic AMI connection restart on 3 missed pings