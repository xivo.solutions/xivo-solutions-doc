.. _install:

*********************
Installing the System
*********************

Install XiVO by using a minimal Debian installation and the XiVO installation script as described here.

The debian host may run on virtual (QEMU/KVM, VirtualBox, ...) or physical machines. That said, since
Asterisk is sensitive to timing issues, you should get better results by installing XiVO on real hardware.

.. warning:: By default XiVO installation will pre-empt network subnets 172.17.0.0/16 and 172.18.1.0/24.
    If these subnets are already used, some manual steps will be needed to be able to install XiVO.
    These steps are not described here.

Please refer to the section :ref:`Troubleshooting <troubleshooting>` if ever you have errors during the installation.

.. warning:: XiVO used to be installable using a dedicated ISO image/PXE. This has been deprecated with Maia (2024.05) so we may focus on the install script.

Requirements
============

The latest XiVO LTS requires a **64-bit** **Debian 12 (Bookworm)**. It is strongly advised to install on with a clean and minimal installation of that distribution.
Retrieve the latest installation image for Debian **Bookworm** at https://www.debian.org/releases/bookworm/debian-installer.

The installed Debian must:

* not have caps in the hostname
* use the architecture ``amd64``
* have a default locale ``en_US.UTF-8``
* use ``ext4`` filesystem (for compatibility with docker overlay2 storage driver)
* use legacy network interface naming ``eth#``. To change the network interface naming to ``eth#`` use this procedure:

  * Edit ``/etc/default/grub``, find line ``GRUB_CMDLINE_LINUX`` and set it to ``GRUB_CMDLINE_LINUX="net.ifnames=0"``
  * Run ``update-grub``
  * Edit interface names in ``/etc/network/interfaces``
  * Reboot the machine

Installation
============

.. note:: If your server needs a proxy to access Internet, configure the proxy for ``apt``, ``wget`` and ``curl`` as documented in :ref:`system_proxy`.

Once you have your Debian properly installed, download the XiVO installation script and make it executable::

   wget http://mirror.xivo.solutions/xivo_install.sh
   chmod +x xivo_install.sh

Then run it with::

   ./xivo_install.sh -a 2024.10-latest

At the end of the installation, you can continue by running the :ref:`configuration
wizard. <configuration_wizard>`


Older versions
==============

The installation script can also be used to install an :ref:`archive version <archive-version>` of
XiVO (14.18 or later only). For example, if you want to install XiVO 2020.18-latest::

   ./xivo_install.sh -a 2020.18-latest

When installing an archive version, note that:

* older versions of XiVO may require older versions of Debian too
* versions 14.18 to 15.19 of XiVO can only be installed on a Debian 7 (wheezy) system
* the 64-bit versions of XiVO are only available starting from 15.16

You may also install development versions of XiVO with this script. These versions may be unstable
and should not be used on a production server. Please refer to the usage of the script::

   ./xivo_install.sh -h
