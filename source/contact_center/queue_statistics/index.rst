.. _queue_statistics:

****************
Queue statistics
****************

Introduction
============

| XiVO generates statistics, based on the queue logs computed by asterisk, on any calls and calls attempt to the queues
| Statistics computed in real time are available in the ccmanager (see :ref:`ccmanager-queue-statistics`).
| Statistics reports can be generated using SpagoBI (see spagobi_).

Schema
======

It summarizes how we sort and name every call to a queue, reaching different states.

.. figure:: queue_stats_vocabulary_EN.drawio.png


| These states are based on asterisk queue logs event types, you should find more infos about them here : https://docs.asterisk.org/Operation/Logging/Queue-Logs/?h=queue+log
| The CCM icon means the value is readable on the CC Manager (see :ref:`ccmanager-queue-statistics`).

.. _spagobi:

SpagoBI
=======

.. warning:: 
  Because the SpagoBI technology is growing old and does not receive open-source updates, we won't build our SpagoBI image after the one from Maia: 2024.05.latest.
  That image will still work just as before for further releases, starting with Naos.

Configure SpagoBI on http://XIVOCC_IP/SpagoBI (by default login: biadmin, password: biadmin).
Replace XIVOCC_IP by the CC VM's IP or the FQDN.

Update default language
-----------------------

#. Go to "⚙ Resources" > "Configuration management"
#. In the "Select Category" field, chose "LANGUAGE_SUPPORTED"
#. change value of the label "SPAGOBI.LANGUAGE_SUPPORTED.LANGUAGE.default" in your language : fr,FR , en,US , ...

.. _upload_sample_reports:

Upload Statistics Reports
-------------------------

Are you on a new installation ? Download the sample_reports from
https://gitlab.com/xivocc/sample_reports/-/raw/master/spagobi/qsr_and_statusdb_from_luna.zip
Did you upgrade to luna ? Get the new queue support report from
https://gitlab.com/xivocc/sample_reports/-/raw/master/spagobi/queue_support_report.zip

Import zip file in SpagoBI:

  #. Goto "Repository Management" -> "Import/Export"
  #. Click on "Browse/Choose your file" and choose the previously downloaded file
  #. Click on "Import" icon
  #. Click next with default options until you are asked to override metadata, set **Yes** as shown in screen below

    .. figure:: reportsoverride.png

You can now browse the reports in *Document->Rapports->Exemples>FA*.

.. image:: queue_support.png

Use the database status report to check if replication and reporting generation is working :

.. figure:: datastatus.png

Agents Statistics
-----------------

The agent statistics report computes the same statistics as the queue statistics report, but the focus is made on the Agent itself rather than on the queue.
There are some subtle differences.

Vocabulary
^^^^^^^^^^

.. figure:: Agent_Stats_Vocabulary_EN.drawio.png

A call may be submitted to an agent several times. Thus a call 'not answered' by an agent may still be answered by another.
This may be more explicit through an example below.

Example
^^^^^^^

.. figure:: Agent_Stats_Example.drawio.png

An offered call can be submitted several times.

Installation
^^^^^^^^^^^^

Retrieve the report under https://gitlab.com/xivocc/sample_reports/-/raw/master/spagobi/agent_support.zip


Import zip file in SpagoBI:

  #. Goto "Repository Management" -> "Import/Export"
  #. Click on "Browse/Choose your file" and choose the previously downloaded file
  #. Click on "Import" icon
  #. Click next with default options until you are asked to override metadata, set **Yes** as shown in screen below

    .. figure:: reportsoverride.png

Report
^^^^^^

You can now browse the reports in *Document->Rapports->Exemples>Agent*.

.. figure:: agent_support.png
