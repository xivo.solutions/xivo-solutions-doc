**********************
Upgrade Kuma to Luna
**********************

In this section is listed the manual steps to do when migrating from Kuma to Luna.

.. contents::


Before Upgrade
==============

On XiVO PBX
-----------

UC Addon
^^^^^^^^


On XiVO CC
----------


After Upgrade
=============

On XIVO PBX
-----------

Agid dockerization: email configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In Luna xivo-agid service was dockerized, which affected email exchange. To make sure that mail exchange will be
working, please check differences between base template and customized templated for mail configuration and make sure
you include `#XIVO_DOCKER_NET#` in your custom templates (in the section `mynetworks`).
In case of XDS installation, you need to do this step also on every MDS.

    .. code-block:: bash

        BASE_TEMPLATE_FOR_MAIL_CONFIG=/usr/share/xivo-config/templates/mail/etc/postfix/main.cf
        CUSTOMIZED_TEMPLATE_FOR_MAIL_CONFIG=/etc/xivo/custom-templates/mail/etc/postfix/main.cf
        vimdiff "$BASE_TEMPLATE_FOR_MAIL_CONFIG" "$CUSTOMIZED_TEMPLATE_FOR_MAIL_CONFIG"

        # Make the changes to CUSTOMIZED_TEMPLATE_FOR_MAIL_CONFIG and apply them using following script

        xivo-update-config

UC Addon
^^^^^^^^


On XiVO CC
----------

