.. _xivocc-upgrade_script:

*********************
xivocc-upgrade script
*********************

Usage
=====

.. note::
   * You can't use xivocc-upgrade if XiVOCC is split on multiple servers

This script will update *XiVOCC* and restart all services.

There are 2 options you can pass to xivo-upgrade:

* ``-d`` to only download packages without installing them. **This will still upgrade the package containing xivo-upgrade and xivo-service**.
* ``-f`` to force upgrade, without asking for user confirmation


