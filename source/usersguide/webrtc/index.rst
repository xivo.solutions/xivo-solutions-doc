.. _webrtc_requirements:

******************
WebRTC Environment
******************

One can use WebRTC with *XiVO PBX* and *XiVO CC* in the following environment:

* LAN network (currently no support for WAN environment),
* with the:

  * *UC Assistant* or *CC Agent* with Chrome browser version **85** or later
  * or *Desktop Application*

Requirements
============

The **requirements** are:

* to have a microphone and headphones for your PC,
* to configure your *XiVO PBX*:

  * configure :ref:`asterisk_http_server`,
  * and then create users with a WebRTC line (see: :ref:`configure_user_with_webrtc_line`),

* have a SSL/TLS certificate signed by a certification authority installed on the nginx of *XiVO CC* (see: :ref:`webrtc-ssl-cert`),
* and use *https*:

  * *UC Assistant*: you must connect to the *UC Assistant* via `https` protocol,
  * *Desktop Application*: you must check *Protocol -> Secure* in the application parameters.

.. note:: Currently you can not have a user configured for both WebRTC and a phone set at the same time.

WebRTC Features
===============

The *UC Assistant*, *CC Agent* and *Switchboard* can be used by users with WebRTC configuration, without physical phone.

For configuration and requirements, see :ref:`webrtc_requirements`.


`*55` (echo test)
-----------------

To test your microphone and speaker, you may call the echo test application. After a welcome message, the application will
echo everything what you speak.

 1. Dial the `*55` number.
 2. You should hear the "Echo" announcement.
 3. After the announcement, you will hear back everything you say.
     If you hear what you are saying it means that your microphone and speakers are working.
 4. Press `#` or hangup to exit the echo test application.

.. _headset_call_control:

Headset call control (WebHID)
-----------------------------

.. important:: 
      * This feature works only with **Poly and Jabra headsets** labelled as **UC** and supporting **WebHID** on the constructor website for now. You can contact the company to talk about additional brands support.
      * Your headset **must be up to date**, and the update can be done through their respective brand software. 

To enable it, go in the application menu, then click on the headset call control button.  
A popup will open so you can pick your headset in the list. Note that **only devices from the supported brands will be displayed in the list**.

+----------------------------------+------------------------------+
| Enabling on UC Assistant         | Enabling on CCAgent & POPC   |
+----------------------------------+------------------------------+
| .. figure:: menu_ucassistant.gif | .. figure:: menu_ccagent.gif |
+----------------------------------+------------------------------+

When enabled, the headset call control allows an user to answer and hangup using his headset, either in the web assistant or in the desktop application.  
You need to have your headset **connected using it's dongle** (or through an usb cable). **The feature does not work through direct bluetooth**.  

When the headset is correctly connected to your application, you will have a visual hint displayed at the top of your application (green blinking / headset).  

+--------------------------------------+-----------------------------------------+
| Headset control hint on UC Assistant | Headset control hint on CC Agent & POPC |
+--------------------------------------+-----------------------------------------+
| .. figure:: blinking_ucassistant.gif | .. figure:: blinking_ccagent.gif        |
|     :scale: 55%                      |     :scale: 80%                         |
+--------------------------------------+-----------------------------------------+

Depending on the case, pushing the phone action button or the play/pause button of your headset (often placed at the center of an earpiece) should do the followings actions :

* if your softphone is ringing, you will answer
* if you are in a call or a conference, you will hangup

.. note:: If the last headset you used is still connected the next time you use your application, then headset call control will be **re-enabled automatically**.

To make this feature work on Linux, you will need to set read permission (or more) over your hidraw driver.
Here is a way to do it:

.. code:: bash

    echo 'KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0644"' | sudo tee /etc/udev/rules.d/99-hidraw-readonly.rules
    sudo udevadm control --reload-rules                                
    sudo udevadm trigger

.. _webrtc_ringtone_and_device:

Ringing device and ringing tone
-------------------------------

When receiving a call, your computer will play a ringing sound. However, you can choose to play this sound on a separate audio device than the default selected by your operating system. For example, on a configuration with a headset, this device may be your default device but you can override this selection to play the ringing sound on your computer instead.


You can also choose to change your ringtone sound, both those options are available in the respective menu of the UCAssistant, CCAgent, and Switchboard, on the top right corner.

.. figure:: ringing-device-selection.png
   :scale: 100%

This feature is only available when using a WebRTC line.

.. _webrtc_mute_self:

Mute your microphone
--------------------

Being a webrtc user allows you to mute your microphone while being in a call with somebody. Clicking this button again will unmute the microphone. Clicking directly on the volume meter will have the same effect as clicking on the mute button. The little microphone icon in the volume meter will change according to your mute state.

On UC Assistant :

.. figure:: ucassistant-call-mute.png

On CC Agent and Switchboard :

.. figure:: ccagent-call-mute.png

.. _webrtc_volume_indication:

Volume indication
-----------------

.. figure:: volume_meter.png

Two progress bars show the volume level of the speaker and the microphone. It certifies that the audio flow has been sent.

.. _webrtc_sound_detection:

Sound detection
---------------

.. figure:: microphone_not_working.png

During the first five seconds of your call, if the application detect that there is no sound coming from your microphone, a small message will appear.

.. _webrtc_quality_detection:

Call quality detection
----------------------

.. figure:: webrtc_audio_quality_issue_msg.png

If the call quality can be impacted by some network issues or server configuration issues, a small message will appear.

The message will stay up to 10 seconds after the audio quality is back to normal.
The advanced statistics in the "show details" section and the play icon colors are updated live.

.. figure:: webrtc_audio_quality_issue_msg_details.png

The current treshold are as following :

Quality goes to medium when something is higher than :
  - Jitter: 50 ms
  - Packet losts: 10%
  - Round trip time: 150 ms

Quality goes to bad when something is higher than :
  - Jitter: 100 ms
  - Packet losts: 20%
  - Round trip time: 300 ms

.. _webrtc_quality_detection_logging:

Call quality server-side feedbacks
----------------------------------

If the quality of a call goes wrong, a live statistics feedback will be sent to the server. 
There will also be an automatic feedback at the end of each call, giving informations on the point where the call quality was at it's worst state.
For more informations on those logs - see :ref:`webrtc_quality_logging`.

Additional details
------------------

Live feedbacks
^^^^^^^^^^^^^^

Live audio quality measurement is taken every 2 seconds. If the quality is bad enough compared to the treshold, they are visible on the user interface as a small yellow message, and are also visible in the browser console.
The statistics that are calculated using those measurements are done using the segment of time inbetween the last measurement and the new one, so it will be the last 2 seconds of the call for each measurement.
The quality feedbacks are sent in xuc server logs only when it deteriorate enough to match the medium or bad quality treshold, and they respect a pause of at least 20 seconds inbetween each one to prevent flooding logs.
For more informations on those logs - see :ref:`webrtc_quality_logging`.

End of call report
^^^^^^^^^^^^^^^^^^

The quality report at the end of each call is always sent to the xuc server logs even if the call went well.
The statistics that are present in this end of call report are equals to the worst numbers found during the call. 
For example, if at some point the user had a spike of 20% packet loss during a few seconds but the rest of the call went well, the report will state that the packet loss spike was 20%.
It is the same with jitter and RTT. It's not an average, it's always the highest detected statistic.
For more informations on those logs - see :ref:`webrtc_quality_logging`.

Upload and download directions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The upload direction is the direction from the user point of view (local outbound flow) sending data to the server (remote inbound flow).
The download direction is the direction from the server point of view (remote outbound flow) sending data to the user (local inbound flow)

.. _webrtc_codec:

Opus Codec
----------

By default WebRTC line uses the Opus codec.

It enhances the audio quality of calls with a lower bitrate (around 20kbps with the current configuration compared to 64kbps for alaw codec).

Limitations
===========

Known limitation are :

* Voice may not be able to hear if your computer have more than 4 network interfaces up at the same time (this can happen if you use virtualization)

.. note::

  | To check if you have more than 4 network interfaces you can type following command:
  | ``ls /sys/class/net``

  | Then just use:
  | ``ifdown <ifname>``

  | This will switch off network interface not required to make your call.

Additional chrome WebRTC-specific options
==========================================

There are various additional settings used in the code.
They are used to improve audio quality by enabling or disabling chrome WebRTC-specific flags.

.. note::

  | These options are not customisable. They are set in the code.

**Chrome currently supports these audio quality options :**

* **Automatic gain control** :  Adjust voice sound level to make it linear, lowering sound level when the user speaks too loudly.

* **Echo cancellation** : Detect and delete echo coming from the playback of the user's own voice.

* **Noise suppression** : Cancel background noises coming from the user's environment.

* **Highpass filter** : Filters out low frequencies noises (like microphone background buzzing permanent sound).

* **Audio mirroring** : Reflect sound coming from different directions into a focus point (similar to a parabola).

* **Typing noise detection** : Detect and delete keypress sounds.

**The current production code is set as follows :**

* googAutoGainControl is set to *false*
* googAutoGainControl2 is set to *false*
* googEchoCancellation is set to *true*
* googEchoCancellation2 is set to *true*
* googNoiseSuppression is set to *false*
* googNoiseSuppression2 is set to *false*
* googHighpassFilter is set to *false*
* googAudioMirroring is set to *false*
* googTypingNoiseDetection is set to *true*

.. note::

  | This flag used to be valid but is now deprecated :
  | - Ducking : Reduce an audio signal by the presence of another signal (multiple people talking at the same time).
