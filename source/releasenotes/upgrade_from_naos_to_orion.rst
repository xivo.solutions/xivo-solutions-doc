*********************
Upgrade Naos to Orion
*********************

.. contents::

Before Upgrade
==============

On XiVO PBX
-----------

* If you have :ref:`recording_gw_features` configured you have to update the subroutine in the configuration file, see Gateway Configuration section: :ref:`recording_gw_configuration_gwdp`

On XiVO CC
----------

N.A

On MDS
------

N.A

On Edge
-------
N.A

On Meeting Rooms
----------------

N.A

After Upgrade
=============

N.A



