.. _directories:

***********
Directories
***********

Whenever you search a user on XiVO using any of its Web Apps or a registered phone, it uses the module Dird to query sources named Directories.
If you have a custom source, such as a LDAP, that you would like XiVO to know about and rely on, you must first register it as a Directory.
Then you must write the right configuration, so XiVO is able to query it and render the right informations from it.

These are the steps to do so:

* :ref:`Add the Directory itself<directory-creation>`
* :ref:`Add the Directory Definition<directory-definition>`
* :ref:`Register the Directory to Direct Search and/or Reverse lookup<directory-registration>`
* :ref:`Restart Dird<restart-dird>`

There are 5 different :ref:`directory_types`, each having some specific configurations. You will find some exampes in those pages, but you must follow the procedure here.

.. note:: Phone IP should be in the authorized subnet to access the directories. See :ref:`remote-directory`.


.. _directory-creation:

I. Add a Directory
==================

You define where the data source that will become a Directory is located and what is its type.

Default configuration
---------------------

In :menuselection:`Configuration --> Management --> Directories`:

* :guilabel:`Directory name`: the directory name
* :guilabel:`Type`: select among:

    * :ref:`XiVO<xivo-directory>`
    * :ref:`CSV File<csv-file-directory>`
    * :ref:`CSV Web service<csv-web-service-directory>`
    * :ref:`Phonebook<phonebook-directory>`

* :guilabel:`URI`: the data source location
* :guilabel:`Description`: (optional) a description of the directory

.. warning:: Editing the :guilabel:`URI` later requires you to save again the :ref:`Directory Definition <directory-definition>` because the corresponding :guilabel:`URI` on Definition will be broken. After that you shall :ref:`restart Dird<restart-dird>`

LDAPs (specific)
----------------

| LDAP Directories are created differently.
| Under :ref:`LDAP Server<create-ldap-server>` you specify the location of your LDAP, and under :ref:`LDAP Filters<create-ldap-filter>` you set how you navigate it.
| See :ref:`ldap` for adding this Directory, then continue here with the remaining steps starting with :ref:`directory-definition`.

.. _directory-definition:

II. Add a Directory Definition
==============================

Under Definition, you define:

* the Directory Source Name
* the Directory Fields that will be used for the search. Directory Fields are values that shall be available on the source. If you're configuring your own custom source, you know what they are.
* the mapping between the Directory Fields and a the DisplayFilter Fields (see :ref:`display_filter`)

In :menuselection:`Services --> CTI Server --> Directories --> Definitions`:

* :guilabel:`Name`: the Directory Source Name
* :guilabel:`URI`: the data source, one linked to the Directory you defined above will appear
* :guilabel:`Delimiter`: (optional) the field delimiter in the data source
* :guilabel:`Direct match`: the list used to match entries for direct lookup (comma separated)
* :guilabel:`Match reverse directories`: (optional) the list used to match entries for reverse
  lookup (comma separated)
* :guilabel:`Mapped fields`: the mapping between your Directory Fields and the DisplayFilter Fields

  * :guilabel:`Fieldname`: your Directory Fields (the values you want to read from it)
  * :guilabel:`Value`: the associated DisplayFilter Fields

.. important:: The Directory Source Name cannot contain special characters, use a-z, A-Z, 0-9

.. _directory-registration:

III. Register the Directory
===========================

Your Directory is defined but not activated. It has to be selected, through its Directory Source Name, for direct search and/or for reverse lookup.

Register Directory for direct search
------------------------------------

1. In :menuselection:`Services --> CTI Server --> Directories --> Direct directories`.
2. Edit your context (usually default)
3. Select your display filter (usually Display, see :ref:`display_filter`)
4. Add the directories in the :guilabel:`Directories` section.

.. figure:: images/direct_directories.png

Register Directory for reverse lookup
-------------------------------------

See :ref:`reverse_lookup` to have further details about this functionnality.

1. In :menuselection:`Services --> CTI Server --> Directories --> Reverse directories`.
2. Add the directories to include to reverse lookups in the :guilabel:`Related directories` section.

.. figure:: images/reverse_directories.png

.. _restart-dird:

IV. Restart Dird
================

Use *one* ofthese methods:

* :menuselection:`Services --> IPBX --> Control --> Restart Dird server`
* console ``service xivo-dird restart`` on XiVO PBX

You may want to follow closely Dird logs there, so you are sure no Directory fails to reload.

.. _directory_types:

Directory Types
===============

.. toctree::
  xivo
  csv_file
  csv_web_service
  ldap/ldap
  phonebook

.. _display_filter:

Display Filter
==============

Location
--------

| In :menuselection:`Services --> CTI Server --> Directories --> Display filters`.
| Use the default **Display** or create your own on its model.
| The DisplayFilters are chosen by context under :ref:`directory-registration`.

Default
-------

.. figure:: images/display_maia.png

| Fields types and Fields variables name are fixed in the default Display.
| You may update the field title to better match your needs. See :ref:`contact-modal` for the visual rendering.

Attributes
----------

Each line in the display filter will result in a header in your Web Apps.

* :guilabel:`Field title`: text displayed in the header.
* :guilabel:`Field type`: type of the column, this information is used by the Web Apps. (see
  :ref:`type description<dird-integration-views>`)
* :guilabel:`Field name`: names of the variables to map in Directory Definitions to Directory Fields
  reverse_lookup

.. _reverse_lookup:

Reverse lookup
==============

It's possible to do reverse lookups on incoming calls to show a better caller ID name when
the caller is in one of our directories.

Reverse lookup will only be tried if at least one of the following conditions is true:

* The caller ID name is the same as the caller ID number
* The caller ID name is "unknown"

.. important:: Reverse lookup is performed **after** :ref:`caller_number_normalization` (since XiVO 13.11).

To enable reverse lookup, you need to add an entry in :guilabel:`Mapped fields`:

* :guilabel:`Fieldname`: ``reverse``
* :guilabel:`Value`: the header of your data source that you want to see as the caller ID on your
  phone on incoming calls


Example
-------

* :guilabel:`Match reverse directories`:
  ``phonebooknumber.office.number,phonebooknumber.mobile.number,phonebooknumber.home.number``
* :guilabel:`Fieldname`: ``reverse``
* :guilabel:`Value`: ``phonebook.society``

This configuration will show the contact's company name on the caller ID name, when the incoming
call will match office, mobile or home number.

.. figure:: images/xivo_phonebook_reverse.png

   :menuselection:`Services --> CTI Server --> Directories --> Definitions`

Merge Contacts
==============

| Previously, if as a company you had your employees registered in a LDAP but wanting to provide them an internal number on XiVO, you had to create XiVO users for your employees with this number.
| Then when searching for them in the XiVO Web Apps (UC Assistant, CC Agent, POPC), they would appear twice in the search.
| You would see this:

.. figure:: images/unmerged_contacts.png

| Starting with Orion, the XUC Server comes with a way to merge Dird results with a **common email**.
| So instead, you will see this:

.. figure:: images/merged_contacts.png

* Given you have several different registered Directories containing users having common emails. 
* Given those Directories have for **Directory Source Name**: internal, ldap1, phonebook, CsvFileOne
* Define the Merge Order in xivocc's `custom.env` with ``DIRECTORY_SOURCES_PRIORITY`` and the sources you want to merge, separated by ``,`` :

.. code-block:: 

  DIRECTORY_SOURCES_PRIORITY="ldap1,internal,phonebook"

| ``DIRECTORY_INTERNAL_SOURCE`` applies a different order for merging the phone (precisely **contact_1_callable** according to the default Display). 
| The common use case is that we trust LDAP informations for everything but when it comes to calling we need the internal number, defined on XiVO.

.. code-block:: 

  DIRECTORY_INTERNAL_SOURCE="internal,phonebook"

* ``DIRECTORY_INTERNAL_SOURCE`` can only contain members of ``DIRECTORY_SOURCES_PRIORITY``. Any other member won't be read.
* ``DIRECTORY_INTERNAL_SOURCE`` merging happen after the ``DIRECTORY_SOURCES_PRIORITY`` merging, updating the object.

.. important:: If ``DIRECTORY_SOURCES_PRIORITY`` is not provided nothing is done. Duplicates are then maintained.

.. important:: If a Source provides several times a contact with the same email (for instance several people use a service email), then these won't be merged.

.. important:: You need to restart XUC for this configuration to apply: **xivocc-dcomp up -d xuc**

.. warning:: The merge appears only on xivo-dird results. If the search term does not lead xivo-dird to retrieve all contacts you want to merge, the merge will be uncomplete. An example of this will be searching contacts by service name, where most likely, only the ldap entries will be found by xivo-dird and not be mergeable with their internal counterpart. 


Advanced Dird Configuration
===========================

* :ref:`Advanced Dird Configration <dird-configuration-file>`.
