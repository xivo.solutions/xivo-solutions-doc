.. _upgrade_asterisk_latest:

##########################
Asterisk upgrade procedure
##########################

Introduction
************

There are three distributions available for each release (since release 2017.03).

For XiVO production repositories

::

	deb http://mirror.xivo.solutions/debian/ xivo-kuma main
	deb http://mirror.xivo.solutions/debian/ xivo-kuma-candidate main
	deb http://mirror.xivo.solutions/debian/ xivo-kuma-oldstable main

* The distribution **xivo-LTS** contains the current stable version of *XiVO PBX* and Asterisk.
* The distribution **candidate** contains only Asterisk in higher version than the current.
* The distribution **oldstable** contains *all* previous stable Asterisk versions : therefore you can always fallback to one of the previous stable version.

Example
-------

+-------------------------------+------------------------+---------+---------------------------------------------------------+
| Repository                    | Distribution           | Section | Content                                                 |
+===============================+========================+=========+=========================================================+
| mirror.xivo.solutions/debian  | xivo-kuma-candidate    | main    | latest-built-and-tested-version (e.g. asterisk-20.11.0) |
+-------------------------------+------------------------+---------+---------------------------------------------------------+
| mirror.xivo.solutions/debian  | xivo-kuma              | main    | most-stable-known-version (e.g. asterisk-20.6.0)        |
+-------------------------------+------------------------+---------+---------------------------------------------------------+
| mirror.xivo.solutions/debian  | xivo-kuma-oldstable    | main    | all-old-stable-known-version (e.g. asterisk-20.3.0)     |
+-------------------------------+------------------------+---------+---------------------------------------------------------+

Upgrade to asterisk candidate
*****************************

Before integrating a new version of asterisk in *XiVO PBX*, we first build it and deliver it in the ``xivo-LTS-candidate`` distribution of our
repository.

The goal is to make it available for specific cases (e.g. urgent bugfixes) before shipping it as the default version.
This page explains how to upgrade/downgrade to/from this candidate version.

.. warning:: This is a specific procedure. You should know what you are doing.


Upgrade to candidate version
****************************

.. warning:: This will upgrade your asterisk version and trigger a *restart* of asterisk.
             You should know what you are doing.

#. Edit the xivo sources list file :file:`/etc/apt/sources.list.d/xivo-dist.list` and add the ``xivo-LTS-candidate`` distribution
   (see **last line entry** below):

   .. code-block:: bash
     :emphasize-lines: 4

     # xivo-kuma
     deb http://mirror.xivo.solutions/debian/ xivo-kuma main
     # deb-src http://mirror.xivo.solutions/debian/ xivo-kuma main
     deb http://mirror.xivo.solutions/debian/ xivo-kuma-candidate main

#. Update the packages list::

	apt-get update

#. Check the proposed versions with ``apt-cache policy asterisk``.
   For example it will give you the following result::

    # apt-cache policy asterisk
    asterisk:
      Installed: 8:20.6.0-1~xivo2+deb12+20240522.081947.010dbfa
      Candidate: 8:20.11.0-1~xivo1+deb12+20241211.170138.e1c1701
      Version table:
         8:20.11.0-1~xivo1+deb12+20241211.170138.e1c1701 500
            500 http://mirror.xivo.solutions/debian xivo-kuma-candidate/main amd64 Packages
      *** 8:20.6.0-1~xivo2+deb12+20240522.081947.010dbfa 500
            500 http://mirror.xivo.solutions/debian xivo-kuma/main amd64 Packages
            100 /var/lib/dpkg/status
         8:20.3.0-1~xivo1+deb12+20240311.170138.ea14f89 500
            500 http://mirror.xivo.solutions/debian xivo-kuma-oldstable/main amd64 Packages


#. Install the new version (install also ``asterisk-dbg`` package if applicable)::

	 apt-get -t xivo-LTS-candidate install asterisk


#. Restart the services (if you have a *XiVO CC*, you should restart its services too)::

    xivo-service restart

**Note:** The priority will prevent installing asterisk from ``xivo-LTS-candidate`` whenever running **apt-get upgrade** or **xivo-upgrade**.

Downgrade to oldstable version
******************************

.. warning:: This will downgrade your asterisk version and trigger a *restart* of asterisk.
             You should know what you are doing.


#. Edit the xivo sources list file :file:`/etc/apt/sources.list.d/xivo-dist.list` and add the ``xivo-LTS-oldstable`` distribution
   (see **last line entry** below):

   .. code-block:: bash
     :emphasize-lines: 4

     # xivo-kuma
     deb http://mirror.xivo.solutions/debian/ xivo-kuma main
     # deb-src http://mirror.xivo.solutions/debian/ xivo-kuma main
     deb http://mirror.xivo.solutions/debian/ xivo-kuma-oldstable main

#. Update the sources list::

    apt-get update

#. Check the proposed versions with ``apt-cache policy asterisk``.
   For example it will give you the following result::

    # apt-cache policy asterisk
    asterisk:
      Installed: 8:20.6.0-1~xivo2+deb12+20240522.081947.010dbfa
      Candidate: 8:20.11.0-1~xivo1+deb12+20241211.170138.e1c1701
      Version table:
         8:20.11.0-1~xivo1+deb12+20241211.170138.e1c1701 500
            500 http://mirror.xivo.solutions/debian xivo-kuma-candidate/main amd64 Packages
      *** 8:20.6.0-1~xivo2+deb12+20240522.081947.010dbfa 500
            500 http://mirror.xivo.solutions/debian xivo-kuma/main amd64 Packages
            100 /var/lib/dpkg/status
         8:20.3.0-1~xivo1+deb12+20240311.170138.ea14f89 500
            500 http://mirror.xivo.solutions/debian xivo-kuma-oldstable/main amd64 Packages


#. And downgrade the version by giving the version in the **oldstable** distribution of the repository.
   With the example below (do the same with ``asterisk-dbg`` if applicable)::

    apt-get install asterisk='8:20.3.0-1~xivo1+deb12+20240311.170138.ea14f89'

#. Restart the services (if you have a *XiVO CC*, you should restart its services too)::

    xivo-service restart

Return to the current version
*****************************

.. warning:: This will return your asterisk version to the current most stable version and trigger a *restart* of asterisk.
             You should know what you are doing.

#. Change the source lists with ``xivo-LTS``::

    xivo-dist xivo-kuma

#. Check the proposed versions with ``apt-cache policy asterisk``.
   For example it will give you the following result::

    # apt-cache policy asterisk
    asterisk:
      Installed: 8:20.3.0-1~xivo1+deb12+20240311.170138.ea14f89
      Candidate: 8:20.11.0-1~xivo1+deb12+20241211.170138.e1c1701
      Version table:
         8:20.11.0-1~xivo1+deb12+20241211.170138.e1c1701 500
            500 http://mirror.xivo.solutions/debian xivo-kuma-candidate/main amd64 Packages
         8:20.6.0-1~xivo2+deb12+20240522.081947.010dbfa 500
            500 http://mirror.xivo.solutions/debian xivo-kuma/main amd64 Packages
      *** 8:20.3.0-1~xivo1+deb12+20240311.170138.ea14f89 500
            500 http://mirror.xivo.solutions/debian xivo-kuma-oldstable/main amd64 Packages
            100 /var/lib/dpkg/status

#. And install the version.
   With the example below (do the same with ``asterisk-dbg`` if applicable)::

    apt-get -t xivo-LTS install asterisk


#. Restart the services (if you have a *XiVO CC*, you should restart its services too)::

    xivo-service restart
